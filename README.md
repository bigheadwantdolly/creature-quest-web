# Overview
Below are the instructions to setup, build, deploy and run unit tests. Ideally, more of this would be confined to the docker container, so that there is less dependency on the host OS, but this works for now.

Since this is a PHP project, you can obviously "deploy" once, and all future modifications you make will be immediately accessible in your browser.
# Setup Instructions
* Install `docker` <a href="https://docs.docker.com/install/">Here!</a>
* Install `php 7.2` <a href="https://secure.php.net/downloads.php">Here!</a>
* Install `composer`
* Install `node`
* Make a `hosts` entry on your machine to have `creature-quest.localhost` resolve to `127.0.0.1`
# Build Instructions
* Run `composer install` in the root directory
* Run `npm install` in the root directory
* Run `npm run dev` in the root directory
# Deployment Instructions
* Run `docker-compose up` in the `docker` directory
# PHP Unit Tests
* Run `docker exec -ti <docker container id> /bin/bash`
* Run `./bin/phpunit`
# JavaScript Unit Tests
Currently there are no unit tests for the JavaScript codebase. There has not been any serious investigation into which unit testing framework could be used that supports pure JavaScript.
# Accessing the Website
Navigate to `http://creature-quest.localhost:8000` in your browser of choice. The port definition can be found in the `docker-compose.yml` file within the project.