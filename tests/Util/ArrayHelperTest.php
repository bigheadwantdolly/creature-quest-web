<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 12:07 PM
 */

namespace App\Tests\Util;

use App\Util\ArrayHelper;
use PHPUnit\Framework\TestCase;

class ArrayHelperTest extends TestCase {

    /**
     * @var array
     */
    private $array;
    /**
     * @var ArrayHelper
     */
    private $arrayHelper;

    protected function setUp() {
        $this->arrayHelper = new ArrayHelper();
        $this->array = [
            0 => 'test',
            'alpha' => 'test two'
        ];
    }

    protected function tearDown() {
        $this->arrayHelper = null;
    }

    public function testReturnsDefaultDefaultValue(): void {
        $index = 'notreal';
        $actual = $this->arrayHelper::getValue($this->array, $index);
        $expected = null;

        $this->assertEquals($expected, $actual);
    }

    public function testReturnsExpectedValueForIntegerIndex(): void {
        $index = 0;
        $actual = $this->arrayHelper::getValue($this->array, $index);
        $expected = $this->array[$index];

        $this->assertEquals($expected, $actual);
    }

    public function testReturnsExpectedValueForStringIndex(): void {
        $index = 'alpha';
        $actual = $this->arrayHelper::getValue($this->array, $index);
        $expected = $this->array[$index];

        $this->assertEquals($expected, $actual);
    }

    public function testReturnsProvidedDefaultValue(): void {
        $index = 'notreal';
        $defaultValue = 'test';
        $actual = $this->arrayHelper::getValue($this->array, $index, $defaultValue);
        $expected = $defaultValue;

        $this->assertEquals($expected, $actual);
    }

}