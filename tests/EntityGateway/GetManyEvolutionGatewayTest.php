<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/30/18
 * Time: 8:43 PM
 */

namespace App\Tests\EntityGateway;

use App\ApiRequest\ApiRequestConfigurationFactory;
use App\ApiRequest\IApiReadRequest;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\EntityFactory;
use App\Entity\IEntityFactory;
use App\EntityGateway\GetManyEvolutionGateway;
use App\EntityGateway\IGetManyEvolutionGateway;
use App\EntityMapper\EvolutionMapper;
use App\EntityMapper\RarityMapper;
use App\EntityMapper\SizeMapper;
use PHPUnit\Framework\TestCase;

class GetManyEvolutionGatewayTest extends TestCase {

    /**
     * @var IApiReadRequest
     */
    private $apiRequest;
    /**
     * @var IApiRequestConfigurationFactory
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var IEntityFactory
     */
    private $entityFactory;
    /**
     * @var EvolutionMapper
     */
    private $entityMapper;
    /**
     * @var IGetManyEvolutionGateway
     */
    private $getManyEntityGateway;
    /**
     * @var array[]
     */
    private $rarities;
    /**
     * @var array[]
     */
    private $sizes;

    protected function setUp() {
        $this->apiRequest = $this->createMock(IApiReadRequest::class);
        $this->apiRequestConfigurationFactory = new ApiRequestConfigurationFactory();
        $this->entityFactory = new EntityFactory();
        $this->entityMapper = new EvolutionMapper(
            $this->entityFactory,
            new RarityMapper($this->entityFactory),
            new SizeMapper($this->entityFactory)
        );

        $this->rarities = [
            [
                RarityMapper::$SourceKeyId => '12345',
                RarityMapper::$SourceKeyName => 'Mr Rarity'
            ],
            [
                RarityMapper::$SourceKeyId => '23456',
                RarityMapper::$SourceKeyName => 'Mrs Rarity'
            ]
        ];

        $this->sizes = [
            [
                SizeMapper::$SourceKeyId => '12345',
                SizeMapper::$SourceKeyName => 'Mr Size'
            ],
            [
                SizeMapper::$SourceKeyId => '23456',
                SizeMapper::$SourceKeyName => 'Mrs Size'
            ]
        ];

        $apiRequestResponse = [
            'evolutions' => [
                [
                    EvolutionMapper::$SourceKeyId => '12345',
                    EvolutionMapper::$SourceKeyCardinality => 1,
                    EvolutionMapper::$SourceKeyRarities => [
                        $this->rarities[0]
                    ],
                    EvolutionMapper::$SourceKeySizes => [
                        $this->sizes[0]
                    ]
                ],
                [
                    EvolutionMapper::$SourceKeyId => '23456',
                    EvolutionMapper::$SourceKeyCardinality => 2,
                    EvolutionMapper::$SourceKeyRarities => [
                        $this->rarities[1]
                    ],
                    EvolutionMapper::$SourceKeySizes => [
                        $this->sizes[1]
                    ]
                ],
                [
                    EvolutionMapper::$SourceKeyId => '34567',
                    EvolutionMapper::$SourceKeyCardinality => 3,
                    EvolutionMapper::$SourceKeyRarities => [
                        $this->rarities[0]
                    ],
                    EvolutionMapper::$SourceKeySizes => [
                        $this->sizes[1]
                    ]
                ],
                [
                    EvolutionMapper::$SourceKeyId => '45678',
                    EvolutionMapper::$SourceKeyCardinality => 4,
                    EvolutionMapper::$SourceKeyRarities => [
                        $this->rarities[1]
                    ],
                    EvolutionMapper::$SourceKeySizes => [
                        $this->sizes[0]
                    ]
                ],
                [
                    EvolutionMapper::$SourceKeyId => '56789',
                    EvolutionMapper::$SourceKeyCardinality => 5,
                    EvolutionMapper::$SourceKeyRarities => [
                        $this->rarities[0],
                        $this->rarities[1]

                    ],
                    EvolutionMapper::$SourceKeySizes => [
                        $this->sizes[0],
                        $this->sizes[1]
                    ]
                ]
            ]
        ];
        $this->apiRequest->method('execute')->willReturn($apiRequestResponse);

        $this->getManyEntityGateway = new GetManyEvolutionGateway(
            $this->apiRequest,
            $this->apiRequestConfigurationFactory,
            $this->entityFactory,
            $this->entityMapper
        );
    }

    protected function tearDown() {
        $this->getManyEntityGateway = null;
    }

    public function testReturnsDataWithoutFilter() {
        $actual = count($this->getManyEntityGateway->execute());
        $expected = 5;

        $this->assertEquals($expected, $actual);
    }

}