<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/30/18
 * Time: 8:43 PM
 */

namespace App\Tests\EntityGateway;

use App\Entity\Color;
use App\EntityGateway\GetOneColorGateway;
use App\EntityGateway\IGetManyColorGateway;
use App\EntityGateway\IGetOneColorGateway;
use App\EntityMapper\ColorMapper;
use PHPUnit\Framework\TestCase;

class GetOneColorGatewayTest extends TestCase {

    /**
     * @var Color[]
     */
    private $colors;
    /**
     * @var ColorMapper
     */
    private $getManyEntityGateway;
    /**
     * @var IGetOneColorGateway
     */
    private $getOneEntityGateway;

    protected function setUp() {
        $this->getManyEntityGateway = $this->createMock(IGetManyColorGateway::class);
        $this->colors = [];

        $color = new Color();
        $color->setId('12345');
        $color->setName('Mr Color');
        $this->colors[] = $color;

        $this->getManyEntityGateway->method('execute')->willReturn($this->colors);

        $this->getOneEntityGateway = new GetOneColorGateway($this->getManyEntityGateway);
    }

    protected function tearDown() {
        $this->getOneEntityGateway = null;
    }

    public function testReturnsExpectedData() {
        $actual = $this->getOneEntityGateway->getById($this->colors[0]->getId());

        $this->assertEquals($this->colors[0], $actual);
    }

}