<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/30/18
 * Time: 8:43 PM
 */

namespace App\Tests\EntityGateway;

use App\ApiRequest\ApiRequestConfigurationFactory;
use App\ApiRequest\IApiReadRequest;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\EntityFactory;
use App\Entity\IEntityFactory;
use App\EntityGateway\GetManyCreatureGateway;
use App\EntityGateway\IGetManyCreatureGateway;
use App\EntityMapper\AttackMapper;
use App\EntityMapper\ColorMapper;
use App\EntityMapper\CreatureMapper;
use App\EntityMapper\EvolutionMapper;
use App\EntityMapper\LevelMapper;
use App\EntityMapper\LevelRequirementMapper;
use App\EntityMapper\RarityMapper;
use App\EntityMapper\SizeMapper;
use PHPUnit\Framework\TestCase;

class GetManyCreatureGatewayTest extends TestCase {

    /**
     * @var IApiReadRequest
     */
    private $apiRequest;
    /**
     * @var IApiRequestConfigurationFactory
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var IEntityFactory
     */
    private $entityFactory;
    /**
     * @var CreatureMapper
     */
    private $entityMapper;
    /**
     * @var IGetManyCreatureGateway
     */
    private $getManyEntityGateway;

    protected function setUp() {
        $this->apiRequest = $this->createMock(IApiReadRequest::class);
        $this->apiRequestConfigurationFactory = new ApiRequestConfigurationFactory();
        $this->entityFactory = new EntityFactory();
        $this->entityMapper = new CreatureMapper(
            $this->entityFactory,
            new AttackMapper($this->entityFactory),
            new ColorMapper($this->entityFactory),
            new EvolutionMapper(
                $this->entityFactory,
                new RarityMapper($this->entityFactory),
                new SizeMapper($this->entityFactory)
            ),
            new LevelMapper(
                $this->entityFactory,
                new LevelRequirementMapper(
                    $this->entityFactory,
                    new EvolutionMapper(
                        $this->entityFactory,
                        new RarityMapper($this->entityFactory),
                        new SizeMapper($this->entityFactory)
                    ),
                    new SizeMapper($this->entityFactory)
                )
            ),
            new RarityMapper($this->entityFactory),
            new SizeMapper($this->entityFactory)
        );

        $apiRequestResponse = [
            'creatures' => [
                [
                    CreatureMapper::$SourceKeyId => '12345',
                    CreatureMapper::$SourceKeyName => 'Mr Creature'
                ]
            ]
        ];
        $this->apiRequest->method('execute')->willReturn($apiRequestResponse);

        $this->getManyEntityGateway = new GetManyCreatureGateway(
            $this->apiRequest,
            $this->apiRequestConfigurationFactory,
            $this->entityFactory,
            $this->entityMapper
        );
    }

    protected function tearDown() {
        $this->getManyEntityGateway = null;
    }

    public function testReturnsExpectedData() {
        $actual = count($this->getManyEntityGateway->execute());
        $expected = 1;

        $this->assertEquals($expected, $actual);
    }

}