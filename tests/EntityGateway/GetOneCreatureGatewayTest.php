<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/30/18
 * Time: 8:43 PM
 */

namespace App\Tests\EntityGateway;

use App\Entity\Creature;
use App\EntityGateway\GetOneCreatureGateway;
use App\EntityGateway\IGetManyCreatureGateway;
use App\EntityGateway\IGetOneCreatureGateway;
use App\EntityMapper\CreatureMapper;
use PHPUnit\Framework\TestCase;

class GetOneCreatureGatewayTest extends TestCase {

    /**
     * @var Creature[]
     */
    private $creatures;
    /**
     * @var CreatureMapper
     */
    private $getManyEntityGateway;
    /**
     * @var IGetOneCreatureGateway
     */
    private $getOneEntityGateway;

    protected function setUp() {
        $this->getManyEntityGateway = $this->createMock(IGetManyCreatureGateway::class);
        $this->creatures = [];

        $creature = new Creature();
        $creature->setId('12345');
        $creature->setName('Mr Creature');
        $this->creatures[] = $creature;

        $this->getManyEntityGateway->method('execute')->willReturn($this->creatures);

        $this->getOneEntityGateway = new GetOneCreatureGateway($this->getManyEntityGateway);
    }

    protected function tearDown() {
        $this->getOneEntityGateway = null;
    }

    public function testReturnsExpectedData() {
        $actual = $this->getOneEntityGateway->getById($this->creatures[0]->getId());

        $this->assertEquals($this->creatures[0], $actual);
    }

}