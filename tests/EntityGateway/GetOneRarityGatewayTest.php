<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/30/18
 * Time: 8:43 PM
 */

namespace App\Tests\EntityGateway;

use App\Entity\Rarity;
use App\EntityGateway\GetOneRarityGateway;
use App\EntityGateway\IGetManyRarityGateway;
use App\EntityGateway\IGetOneRarityGateway;
use App\EntityMapper\RarityMapper;
use PHPUnit\Framework\TestCase;

class GetOneRarityGatewayTest extends TestCase {

    /**
     * @var RarityMapper
     */
    private $getManyEntityGateway;
    /**
     * @var IGetOneRarityGateway
     */
    private $getOneEntityGateway;
    /**
     * @var Rarity[]
     */
    private $rarities;

    protected function setUp() {
        $this->getManyEntityGateway = $this->createMock(IGetManyRarityGateway::class);
        $this->rarities = [];

        $rarity = new Rarity();
        $rarity->setId('12345');
        $rarity->setName('Mr Rarity');
        $this->rarities[] = $rarity;

        $this->getManyEntityGateway->method('execute')->willReturn($this->rarities);

        $this->getOneEntityGateway = new GetOneRarityGateway($this->getManyEntityGateway);
    }

    protected function tearDown() {
        $this->getOneEntityGateway = null;
    }

    public function testReturnsExpectedData() {
        $actual = $this->getOneEntityGateway->getById($this->rarities[0]->getId());

        $this->assertEquals($this->rarities[0], $actual);
    }

}