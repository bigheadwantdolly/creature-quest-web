<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/30/18
 * Time: 8:43 PM
 */

namespace App\Tests\EntityGateway;

use App\Entity\Size;
use App\EntityGateway\GetOneSizeGateway;
use App\EntityGateway\IGetManySizeGateway;
use App\EntityGateway\IGetOneSizeGateway;
use App\EntityMapper\SizeMapper;
use PHPUnit\Framework\TestCase;

class GetOneSizeGatewayTest extends TestCase {

    /**
     * @var SizeMapper
     */
    private $getManyEntityGateway;
    /**
     * @var IGetOneSizeGateway
     */
    private $getOneEntityGateway;
    /**
     * @var Size[]
     */
    private $sizes;

    protected function setUp() {
        $this->getManyEntityGateway = $this->createMock(IGetManySizeGateway::class);
        $this->sizes = [];

        $size = new Size();
        $size->setId('12345');
        $size->setName('Mr Size');
        $this->sizes[] = $size;

        $this->getManyEntityGateway->method('execute')->willReturn($this->sizes);

        $this->getOneEntityGateway = new GetOneSizeGateway($this->getManyEntityGateway);
    }

    protected function tearDown() {
        $this->getOneEntityGateway = null;
    }

    public function testReturnsExpectedData() {
        $actual = $this->getOneEntityGateway->getById($this->sizes[0]->getId());

        $this->assertEquals($this->sizes[0], $actual);
    }

}