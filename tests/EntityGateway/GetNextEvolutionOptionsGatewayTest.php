<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/30/18
 * Time: 8:43 PM
 */

namespace App\Tests\EntityGateway;

use App\ApiRequest\IApiReadRequest;
use App\ApiRequest\IApiReadRequestConfiguration;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\Creature;
use App\EntityGateway\GetNextEvolutionOptionsGateway;
use App\EntityGateway\IGetNextEvolutionOptionsGateway;
use App\EntityMapper\CreatureMapper;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetNextEvolutionOptionsGatewayTest extends TestCase {

    /**
     * @var IApiReadRequest|MockObject
     */
    private $apiRequest;
    /**
     * @var IApiReadRequestConfiguration|MockObject
     */
    private $apiRequestConfiguration;
    /**
     * @var IApiRequestConfigurationFactory|MockObject
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var string[]
     */
    private $apiRequestResponse;
    /**
     * @var Creature
     */
    private $entity;
    /**
     * @var IGetNextEvolutionOptionsGateway
     */
    private $entityGateway;
    /**
     * @var CreatureMapper|MockObject
     */
    private $entityMapper;
    /**
     * @var Creature|MockObject
     */
    private $entityMock;

    protected function setUp() {
        $this->apiRequest = $this->createMock(IApiReadRequest::class);
        $this->apiRequestConfigurationFactory = $this->createMock(IApiRequestConfigurationFactory::class);
        $this->apiRequestConfiguration = $this->createMock(IApiReadRequestConfiguration::class);
        $this->entityMapper = $this->createMock(CreatureMapper::class);
        $this->entityMock = $this->createMock(Creature::class);

        $this->apiRequestConfigurationFactory->method('createReadConfiguration')->willReturn(
            $this->apiRequestConfiguration
        );

        $this->entity = new Creature();
        $this->entity->setId('12345');
        $this->entity->setName('Mr Creature');

        $this->apiRequestResponse = [
            GetNextEvolutionOptionsGateway::ENTITY_DATA_ROOT_KEY => [
                [
                    CreatureMapper::$SourceKeyId => $this->entity->getId(),
                    CreatureMapper::$SourceKeyName => $this->entity->getName()
                ]
            ]
        ];

        $this->apiRequest->method('execute')->willReturn($this->apiRequestResponse);

        $this->entityGateway = new GetNextEvolutionOptionsGateway(
            $this->apiRequest,
            $this->apiRequestConfigurationFactory,
            $this->entityMapper
        );
    }

    protected function tearDown() {
        $this->entityGateway = null;
    }

    public function testApiRequestConfigurationFactoryIsCalled() {
        $colorId = '12345';
        $evolutionId = '23456';
        $sizeId = '34567';
        $parameters = [
            GetNextEvolutionOptionsGateway::COLOR_PARAMETER_KEY => $colorId,
            GetNextEvolutionOptionsGateway::EVOLUTION_PARAMETER_KEY => $evolutionId,
            GetNextEvolutionOptionsGateway::SIZE_PARAMETER_KEY => $sizeId
        ];
        $this->apiRequestConfigurationFactory->expects($this->once())->method('createReadConfiguration')->with(
            GetNextEvolutionOptionsGateway::ENDPOINT_SUFFIX,
            $parameters
        );

        $this->entityGateway->execute($colorId, $evolutionId, $sizeId);
    }

    public function testApiRequestIsMade() {
        $this->apiRequest->expects($this->once())->method('execute')->with($this->apiRequestConfiguration);

        $this->entityGateway->execute(null, null, null);
    }

    public function testEntityMapperIsCalled() {
        $this->entityMapper->expects($this->once())->method('MapMany')->with(
            $this->apiRequestResponse[GetNextEvolutionOptionsGateway::ENTITY_DATA_ROOT_KEY],
            []
        );

        $this->entityGateway->execute(null, null, null);
    }

    public function testReturnsEntityArrayThatIsNotNull() {
        $actual = $this->entityGateway->execute(null, null, null);

        $this->assertNotNull($actual);
    }

}