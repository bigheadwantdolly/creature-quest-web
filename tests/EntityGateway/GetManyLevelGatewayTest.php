<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/30/18
 * Time: 8:43 PM
 */

namespace App\Tests\EntityGateway;

use App\ApiRequest\ApiRequestConfigurationFactory;
use App\ApiRequest\IApiReadRequest;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\EntityFactory;
use App\Entity\IEntityFactory;
use App\EntityGateway\GetManyLevelGateway;
use App\EntityGateway\IGetManyLevelGateway;
use App\EntityMapper\EvolutionMapper;
use App\EntityMapper\LevelMapper;
use App\EntityMapper\LevelRequirementMapper;
use App\EntityMapper\RarityMapper;
use App\EntityMapper\SizeMapper;
use PHPUnit\Framework\TestCase;

class GetManyLevelGatewayTest extends TestCase {

    /**
     * @var IApiReadRequest
     */
    private $apiRequest;
    /**
     * @var IApiRequestConfigurationFactory
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var IEntityFactory
     */
    private $entityFactory;
    /**
     * @var LevelMapper
     */
    private $entityMapper;
    /**
     * @var IGetManyLevelGateway
     */
    private $getManyEntityGateway;
    /**
     * @var array[]
     */
    private $levelRequirements;
    /**
     * @var array[]
     */
    private $rarities;

    protected function setUp() {
        $this->apiRequest = $this->createMock(IApiReadRequest::class);
        $this->apiRequestConfigurationFactory = new ApiRequestConfigurationFactory();
        $this->entityFactory = new EntityFactory();
        $this->entityMapper = new LevelMapper(
            new EntityFactory(),
            new LevelRequirementMapper(
                new EntityFactory(),
                new EvolutionMapper(
                    new EntityFactory(),
                    new RarityMapper(new EntityFactory()),
                    new SizeMapper(new EntityFactory())
                ),
                new SizeMapper(new EntityFactory())
            )
        );

        $this->levelRequirements = [
            [
                LevelRequirementMapper::$SourceKeyId => '12345',
                LevelRequirementMapper::$SourceKeyEvolution => [],
                LevelRequirementMapper::$SourceKeySize => []
            ],
            [
                LevelRequirementMapper::$SourceKeyId => '23456',
                LevelRequirementMapper::$SourceKeyEvolution => [],
                LevelRequirementMapper::$SourceKeySize => []
            ]
        ];

        $apiRequestResponse = [
            'levels' => [
                [
                    LevelMapper::$SourceKeyId => '12345',
                    LevelMapper::$SourceKeyNumber => 15,
                    LevelMapper::$SourceKeyLevelRequirements => [
                        $this->levelRequirements[0]
                    ]
                ],
                [
                    LevelMapper::$SourceKeyId => '23456',
                    LevelMapper::$SourceKeyNumber => 30,
                    LevelMapper::$SourceKeyLevelRequirements => [
                        $this->levelRequirements[1]
                    ]
                ],
                [
                    LevelMapper::$SourceKeyId => '34567',
                    LevelMapper::$SourceKeyNumber => 45
                ]
            ]
        ];
        $this->apiRequest->method('execute')->willReturn($apiRequestResponse);

        $this->getManyEntityGateway = new GetManyLevelGateway(
            $this->apiRequest,
            $this->apiRequestConfigurationFactory,
            $this->entityFactory,
            $this->entityMapper
        );
    }

    protected function tearDown() {
        $this->getManyEntityGateway = null;
    }

    public function testReturnsDataWithoutFilter() {
        $actual = count($this->getManyEntityGateway->execute());
        $expected = 3;

        $this->assertEquals($expected, $actual);
    }

}