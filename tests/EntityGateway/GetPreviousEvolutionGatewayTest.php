<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 7/8/2018
 * Time: 1:18 AM
 */

namespace App\Tests\EntityGateway;

use App\Entity\Evolution;
use App\EntityGateway\GetPreviousEvolutionGateway;
use App\EntityGateway\IGetManyEvolutionGateway;
use App\EntityGateway\IGetPreviousEvolutionGateway;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetPreviousEvolutionGatewayTest extends TestCase {
    /**
     * @var IGetPreviousEvolutionGateway
     */
    private $entityGateway;
    /**
     * @var Evolution[]
     */
    private $evolutions;
    /**
     * @var IGetManyEvolutionGateway|MockObject
     */
    private $getManyEntityGateway;

    protected function setUp() {
        $this->getManyEntityGateway = $this->createMock(IGetManyEvolutionGateway::class);
        $this->evolutions = [];

        $evolution = new Evolution();
        $evolution->setId('12345');
        $evolution->setCardinality(1);
        $this->evolutions[] = $evolution;

        $evolution = new Evolution();
        $evolution->setId('23456');
        $evolution->setCardinality(2);
        $this->evolutions[] = $evolution;

        $evolution = new Evolution();
        $evolution->setId('34567');
        $evolution->setCardinality(3);
        $this->evolutions[] = $evolution;

        $this->getManyEntityGateway->method('execute')->willReturn($this->evolutions);

        $this->entityGateway = new GetPreviousEvolutionGateway($this->getManyEntityGateway);
    }

    protected function tearDown() {
        $this->entityGateway = null;
    }

    public function testGetManyEvolutionGatewayIsCalled() {
        $this->getManyEntityGateway->expects($this->once())->method('execute');

        $this->entityGateway->execute('12345');
    }

    public function testReturnsExpectedEvolution() {
        $actual = $this->entityGateway->execute($this->evolutions[1]->getId());
        $expected = $this->evolutions[0];

        $this->assertEquals($expected, $actual);
    }

    public function testReturnsExpectedNullValue() {
        $actual = $this->entityGateway->execute($this->evolutions[0]->getId());
        $expected = null;

        $this->assertEquals($expected, $actual);
    }

    public function testReturnsExpectedNullValueGivenFalseInput() {
        $actual = $this->entityGateway->execute('666');
        $expected = null;

        $this->assertEquals($expected, $actual);
    }
}