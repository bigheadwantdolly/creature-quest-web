<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/30/18
 * Time: 8:43 PM
 */

namespace App\Tests\EntityGateway;

use App\Entity\Evolution;
use App\EntityGateway\GetOneEvolutionGateway;
use App\EntityGateway\IGetManyEvolutionGateway;
use App\EntityGateway\IGetOneEvolutionGateway;
use App\EntityMapper\EvolutionMapper;
use PHPUnit\Framework\TestCase;

class GetOneEvolutionGatewayTest extends TestCase {

    /**
     * @var Evolution[]
     */
    private $evolutions;
    /**
     * @var EvolutionMapper
     */
    private $getManyEntityGateway;
    /**
     * @var IGetOneEvolutionGateway
     */
    private $getOneEntityGateway;

    protected function setUp() {
        $this->getManyEntityGateway = $this->createMock(IGetManyEvolutionGateway::class);
        $this->evolutions = [];

        $evolution = new Evolution();
        $evolution->setId('12345');
        $evolution->setCardinality(1);
        $this->evolutions[] = $evolution;

        $this->getManyEntityGateway->method('execute')->willReturn($this->evolutions);

        $this->getOneEntityGateway = new GetOneEvolutionGateway($this->getManyEntityGateway);
    }

    protected function tearDown() {
        $this->getOneEntityGateway = null;
    }

    public function testReturnsExpectedData() {
        $actual = $this->getOneEntityGateway->getById($this->evolutions[0]->getId());

        $this->assertEquals($this->evolutions[0], $actual);
    }

}