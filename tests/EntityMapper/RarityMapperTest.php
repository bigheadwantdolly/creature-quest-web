<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 12:07 PM
 */

namespace App\Tests\EntityMapper;

use App\Entity\EntityFactory;
use App\Entity\Rarity;
use App\EntityMapper\IEntityMapper;
use App\EntityMapper\RarityMapper;
use PHPUnit\Framework\TestCase;

class RarityMapperTest extends TestCase {

    /**
     * @var string[][]
     */
    private $entities;

    /**
     * @var IEntityMapper
     */
    private $entityMapper;

    protected function setUp() {
        $this->entityMapper = new RarityMapper(new EntityFactory());
        $this->entities = [
            [
                RarityMapper::$SourceKeyId => '12345',
                RarityMapper::$SourceKeyName => 'common'
            ],
            [
                RarityMapper::$SourceKeyId => '23456',
                RarityMapper::$SourceKeyName => 'rare'
            ],
            [
                RarityMapper::$SourceKeyId => '3456',
                RarityMapper::$SourceKeyName => 'epic'
            ]
        ];
    }

    protected function tearDown() {
        $this->entityMapper = null;
    }

    public function testMapsId(): void {
        $entity = new Rarity();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getId();
        $expected = $this->entities[0][RarityMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsMany(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = count($entities);
        $expected = count($this->entities);

        $this->assertEquals($expected, $actual);
    }

    public function testMapsManyToExpectedType(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = get_class($entities[0]);
        $expected = Rarity::class;

        $this->assertEquals($expected, $actual);
    }

    public function testMapsName(): void {
        $entity = new Rarity();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getName();
        $expected = $this->entities[0][RarityMapper::$SourceKeyName];

        $this->assertEquals($expected, $actual);
    }

}