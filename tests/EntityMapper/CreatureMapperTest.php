<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 12:07 PM
 */

namespace App\Tests\EntityMapper;

use App\Entity\Creature;
use App\Entity\EntityFactory;
use App\EntityMapper\AttackMapper;
use App\EntityMapper\ColorMapper;
use App\EntityMapper\CreatureMapper;
use App\EntityMapper\EvolutionMapper;
use App\EntityMapper\LevelMapper;
use App\EntityMapper\LevelRequirementMapper;
use App\EntityMapper\RarityMapper;
use App\EntityMapper\SizeMapper;
use PHPUnit\Framework\TestCase;

class CreatureMapperTest extends TestCase {

    /**
     * @var string[][]
     */
    private $entities;

    /**
     * @var CreatureMapper
     */
    private $entityMapper;

    protected function setUp() {
        $entityFactory = new EntityFactory();

        $this->entityMapper = new CreatureMapper(
            new EntityFactory(),
            new AttackMapper($entityFactory),
            new ColorMapper($entityFactory),
            new EvolutionMapper($entityFactory, new RarityMapper($entityFactory), new SizeMapper($entityFactory)),
            new LevelMapper(
                $entityFactory,
                new LevelRequirementMapper(
                    $entityFactory,
                    new EvolutionMapper(
                        $entityFactory, new RarityMapper($entityFactory), new SizeMapper($entityFactory)
                    ),
                    new SizeMapper($entityFactory)
                )
            ),
            new RarityMapper($entityFactory),
            new SizeMapper($entityFactory)
        );
        $this->entities = [
            [
                CreatureMapper::$SourceKeyId => '12345',
                CreatureMapper::$SourceKeyName => 'creature one',
                CreatureMapper::$SourceKeyAttackRating => 1,
                CreatureMapper::$SourceKeyDefenseRating => 2,
                CreatureMapper::$SourceKeyHealthPoints => 3,
                CreatureMapper::$SourceKeyLuckRating => 4,
                CreatureMapper::$SourceKeyManaPool => 5,
                CreatureMapper::$SourceKeyPowerRating => 6,
                CreatureMapper::$SourceKeyAttack => [
                    AttackMapper::$SourceKeyId => '12345',
                    AttackMapper::$SourceKeyName => 'mr attack'
                ],
                CreatureMapper::$SourceKeyColor => [
                    ColorMapper::$SourceKeyId => '12345',
                    ColorMapper::$SourceKeyName => 'red'
                ],
                CreatureMapper::$SourceKeyEvolution => [
                    EvolutionMapper::$SourceKeyId => '12345',
                    EvolutionMapper::$SourceKeyCardinality => 1
                ],
                CreatureMapper::$SourceKeyLevel => [
                    LevelMapper::$SourceKeyId => '12345',
                    LevelMapper::$SourceKeyNumber => 15
                ],
                CreatureMapper::$SourceKeyNextCreature => [
                    CreatureMapper::$SourceKeyId => '23456',
                    CreatureMapper::$SourceKeyName => "Next Creature"
                ],
                CreatureMapper::$SourceKeyPreviousCreature => [
                    CreatureMapper::$SourceKeyId => '34567',
                    CreatureMapper::$SourceKeyName => "Previous Creature"
                ],
                CreatureMapper::$SourceKeyRarity => [
                    RarityMapper::$SourceKeyId => '12345',
                    RarityMapper::$SourceKeyName => 'common'
                ],
                CreatureMapper::$SourceKeySize => [
                    SizeMapper::$SourceKeyId => '12345',
                    SizeMapper::$SourceKeyName => 'small'
                ]
            ],
            [
                CreatureMapper::$SourceKeyId => '23456',
                CreatureMapper::$SourceKeyName => 'creature two',
                CreatureMapper::$SourceKeyAttackRating => 1,
                CreatureMapper::$SourceKeyDefenseRating => 2,
                CreatureMapper::$SourceKeyHealthPoints => 3,
                CreatureMapper::$SourceKeyLuckRating => 4,
                CreatureMapper::$SourceKeyManaPool => 5,
                CreatureMapper::$SourceKeyPowerRating => 6,
                CreatureMapper::$SourceKeyAttack => [
                    AttackMapper::$SourceKeyId => '23456',
                    AttackMapper::$SourceKeyName => 'mr attack'
                ],
                CreatureMapper::$SourceKeyColor => [
                    ColorMapper::$SourceKeyId => '23456',
                    ColorMapper::$SourceKeyName => 'blue'
                ],
                CreatureMapper::$SourceKeyEvolution => [
                    EvolutionMapper::$SourceKeyId => '23456',
                    EvolutionMapper::$SourceKeyCardinality => 2
                ],
                CreatureMapper::$SourceKeyLevel => [
                    LevelMapper::$SourceKeyId => '23456',
                    LevelMapper::$SourceKeyNumber => 30
                ],
                CreatureMapper::$SourceKeyNextCreature => [
                    CreatureMapper::$SourceKeyId => '34567',
                    CreatureMapper::$SourceKeyName => "Next Creature"
                ],
                CreatureMapper::$SourceKeyRarity => [
                    RarityMapper::$SourceKeyId => '23456',
                    RarityMapper::$SourceKeyName => 'common'
                ],
                CreatureMapper::$SourceKeySize => [
                    SizeMapper::$SourceKeyId => '23456',
                    SizeMapper::$SourceKeyName => 'small'
                ]
            ],
            [
                CreatureMapper::$SourceKeyId => '34567',
                CreatureMapper::$SourceKeyName => 'creature three',
                CreatureMapper::$SourceKeyAttackRating => 1,
                CreatureMapper::$SourceKeyDefenseRating => 2,
                CreatureMapper::$SourceKeyHealthPoints => 3,
                CreatureMapper::$SourceKeyLuckRating => 4,
                CreatureMapper::$SourceKeyManaPool => 5,
                CreatureMapper::$SourceKeyPowerRating => 6,
                CreatureMapper::$SourceKeyAttack => [
                    AttackMapper::$SourceKeyId => '34567',
                    AttackMapper::$SourceKeyName => 'mr attack'
                ],
                CreatureMapper::$SourceKeyColor => [
                    ColorMapper::$SourceKeyId => '34567',
                    ColorMapper::$SourceKeyName => 'green'
                ],
                CreatureMapper::$SourceKeyEvolution => [
                    EvolutionMapper::$SourceKeyId => '34567',
                    EvolutionMapper::$SourceKeyCardinality => 3
                ],
                CreatureMapper::$SourceKeyLevel => [
                    LevelMapper::$SourceKeyId => '34567',
                    LevelMapper::$SourceKeyNumber => 45
                ],
                CreatureMapper::$SourceKeyRarity => [
                    RarityMapper::$SourceKeyId => '34567',
                    RarityMapper::$SourceKeyName => 'common'
                ],
                CreatureMapper::$SourceKeySize => [
                    SizeMapper::$SourceKeyId => '34567',
                    SizeMapper::$SourceKeyName => 'small'
                ]
            ]
        ];
    }

    protected function tearDown() {
        $this->entityMapper = null;
    }

    public function testMapsAttack(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getAttack()->getId();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyAttack][AttackMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsAttackRating(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getAttackRating();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyAttackRating];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsColor(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getColor()->getId();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyColor][ColorMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsDefenseRating(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getDefenseRating();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyDefenseRating];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsEvolution(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getEvolution()->getId();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyEvolution][EvolutionMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsHealthPoints(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getHealthPoints();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyHealthPoints];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsId(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getId();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsLevel(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getLevel()->getId();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyLevel][LevelMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsLuckRating(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getLuckRating();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyLuckRating];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsManaPool(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getManaPool();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyManaPool];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsMany(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = count($entities);
        $expected = count($this->entities);

        $this->assertEquals($expected, $actual);
    }

    public function testMapsManyToExpectedType(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = get_class($entities[0]);
        $expected = Creature::class;

        $this->assertEquals($expected, $actual);
    }

    public function testMapsName(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getName();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyName];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsNextCreature(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getNextCreature()->getId();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyNextCreature][CreatureMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsPowerRating(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getPowerRating();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyPowerRating];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsPreviousCreature(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getPreviousCreature()->getId();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyPreviousCreature][CreatureMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsRarity(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getColor()->getId();
        $expected = $this->entities[0][CreatureMapper::$SourceKeyRarity][RarityMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsSize(): void {
        $entity = new Creature();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getColor()->getId();
        $expected = $this->entities[0][CreatureMapper::$SourceKeySize][SizeMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

}