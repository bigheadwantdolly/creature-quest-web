<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 12:07 PM
 */

namespace App\Tests\EntityMapper;

use App\Entity\EntityFactory;
use App\Entity\LevelRequirement;
use App\EntityMapper\EvolutionMapper;
use App\EntityMapper\IEntityMapper;
use App\EntityMapper\LevelRequirementMapper;
use App\EntityMapper\RarityMapper;
use App\EntityMapper\SizeMapper;
use PHPUnit\Framework\TestCase;

class LevelRequirementMapperTest extends TestCase {

    /**
     * @var string[][]
     */
    private $entities;

    /**
     * @var IEntityMapper
     */
    private $entityMapper;

    protected function setUp() {
        $this->entityMapper = new LevelRequirementMapper(
            new EntityFactory(),
            new EvolutionMapper(
                new EntityFactory(),
                new RarityMapper(new EntityFactory()),
                new SizeMapper(new EntityFactory())
            ),
            new SizeMapper(new EntityFactory())
        );
        $this->entities = [
            [
                LevelRequirementMapper::$SourceKeyId => '12345',
                LevelRequirementMapper::$SourceKeyEvolution => [
                    EvolutionMapper::$SourceKeyId => '12345',
                    EvolutionMapper::$SourceKeyCardinality => 1
                ],
                LevelRequirementMapper::$SourceKeySize => [
                    SizeMapper::$SourceKeyId => '12345',
                    SizeMapper::$SourceKeyName => 'Mr Size'
                ]
            ],
            [
                LevelRequirementMapper::$SourceKeyId => '12345',
                LevelRequirementMapper::$SourceKeyEvolution => [
                    EvolutionMapper::$SourceKeyId => '23456',
                    EvolutionMapper::$SourceKeyCardinality => 2
                ],
                LevelRequirementMapper::$SourceKeySize => [
                    SizeMapper::$SourceKeyId => '23456',
                    SizeMapper::$SourceKeyName => 'Mrs Size'
                ]
            ],
            [
                LevelRequirementMapper::$SourceKeyId => '12345',
                LevelRequirementMapper::$SourceKeyEvolution => [
                    EvolutionMapper::$SourceKeyId => '34567',
                    EvolutionMapper::$SourceKeyCardinality => 3
                ],
                [
                    SizeMapper::$SourceKeyId => '34567',
                    SizeMapper::$SourceKeyName => 'Just Size'
                ]
            ]
        ];
    }

    protected function tearDown() {
        $this->entityMapper = null;
    }

    public function testMapsEvolution(): void {
        $entity = new LevelRequirement();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getEvolution()->getId();
        $expected = $this->entities[0][LevelRequirementMapper::$SourceKeyEvolution][EvolutionMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsId(): void {
        $entity = new LevelRequirement();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getId();
        $expected = $this->entities[0][LevelRequirementMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsMany(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = count($entities);
        $expected = count($this->entities);

        $this->assertEquals($expected, $actual);
    }

    public function testMapsManyToExpectedType(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = get_class($entities[0]);
        $expected = LevelRequirement::class;

        $this->assertEquals($expected, $actual);
    }

    public function testMapsSize(): void {
        $entity = new LevelRequirement();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getSize()->getId();
        $expected = $this->entities[0][LevelRequirementMapper::$SourceKeySize][SizeMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

}