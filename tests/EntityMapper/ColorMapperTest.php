<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 12:07 PM
 */

namespace App\Tests\EntityMapper;

use App\Entity\Color;
use App\Entity\EntityFactory;
use App\EntityMapper\ColorMapper;
use App\EntityMapper\IEntityMapper;
use PHPUnit\Framework\TestCase;

class ColorMapperTest extends TestCase {

    /**
     * @var string[][]
     */
    private $entities;

    /**
     * @var IEntityMapper
     */
    private $entityMapper;

    protected function setUp() {
        $this->entityMapper = new ColorMapper(new EntityFactory());
        $this->entities = [
            [
                ColorMapper::$SourceKeyId => '12345',
                ColorMapper::$SourceKeyName => 'red'
            ],
            [
                ColorMapper::$SourceKeyId => '23456',
                ColorMapper::$SourceKeyName => 'green'
            ],
            [
                ColorMapper::$SourceKeyId => '3456',
                ColorMapper::$SourceKeyName => 'blue'
            ]
        ];
    }

    protected function tearDown() {
        $this->entityMapper = null;
    }

    public function testMapsId(): void {
        $entity = new Color();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getId();
        $expected = $this->entities[0][ColorMapper::$SourceKeyId];

        $this->assertEquals($expected, $actual);
    }

    public function testMapsMany(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = count($entities);
        $expected = count($this->entities);

        $this->assertEquals($expected, $actual);
    }

    public function testMapsManyToExpectedType(): void {
        $entities = [];
        $this->entityMapper->mapMany($this->entities, $entities);

        $actual = get_class($entities[0]);
        $expected = Color::class;

        $this->assertEquals($expected, $actual);
    }

    public function testMapsName(): void {
        $entity = new Color();
        $this->entityMapper->mapOne($this->entities[0], $entity);

        $actual = $entity->getName();
        $expected = $this->entities[0][ColorMapper::$SourceKeyName];

        $this->assertEquals($expected, $actual);
    }

}