<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 2:42 PM
 */

namespace App\Tests\Entity;

use App\Entity\EntityFactory;
use App\Entity\IEntityFactory;
use App\Tests\TestDouble\Entity\Dummy;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use stdClass;

class EntityFactoryTest extends TestCase {

    /**
     * @var IEntityFactory
     */
    private $entityFactory;

    protected function setUp() {
        $this->entityFactory = new EntityFactory();
    }

    protected function tearDown() {
        $this->entityFactory = null;
    }

    public function testCreatesExpectedEntity() {
        $entity = $this->entityFactory->create(Dummy::class);

        $this->assertThat(get_class($entity), $this->equalTo(Dummy::class));
    }

    public function testThrowsExceptionIfNotEntityType() {
        $object = new stdClass();

        $this->expectException(InvalidArgumentException::class);
        $this->entityFactory->create(get_class($object));
    }
}