<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 12:19 AM
 */

namespace App\Tests\UseCase;

use App\Entity\Level;
use App\EntityGateway\IGetManyLevelGateway;
use App\UseCase\GetLevelsUseCase;
use App\UseCase\GetLevelsUseCaseInput;
use App\UseCase\GetLevelsUseCaseOutput;
use App\UseCase\IGetLevelsUseCase;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetLevelsUseCaseTest extends TestCase {

    /**
     * @var MockObject
     */
    private $entityGateway;
    /**
     * @var IGetLevelsUseCase
     */
    private $useCase;

    protected function setUp() {
        $this->entityGateway = $this->createMock(IGetManyLevelGateway::class);
        $this->useCase = new GetLevelsUseCase($this->entityGateway);
    }

    protected function tearDown() {
        $this->entityGateway = null;
        $this->useCase = null;
    }

    public function testFiltersByEvolution(): void {
        $levels = [
            new Level()
        ];
        $evolutionId = '1';
        $useCaseInput = new GetLevelsUseCaseInput();
        $useCaseOutput = new GetLevelsUseCaseOutput();

        $this->entityGateway->method('execute')->with($this->equalTo($evolutionId), $this->equalTo(null))->willReturn(
            $levels
        );
        $useCaseInput->setEvolutionId($evolutionId);
        $this->useCase->execute($useCaseInput, $useCaseOutput);

        $actual = count($useCaseOutput->getLevels());
        $expected = 1;

        $this->assertEquals($expected, $actual);
    }

    public function testFiltersBySize(): void {
        $levels = [
            new Level()
        ];
        $sizeId = '1';
        $useCaseInput = new GetLevelsUseCaseInput();
        $useCaseOutput = new GetLevelsUseCaseOutput();

        $this->entityGateway->method('execute')->with($this->equalTo(null), $this->equalTo($sizeId))->willReturn(
            $levels
        );
        $useCaseInput->setSizeId($sizeId);
        $this->useCase->execute($useCaseInput, $useCaseOutput);

        $actual = count($useCaseOutput->getLevels());
        $expected = 1;

        $this->assertEquals($expected, $actual);
    }

    public function testOutputsLevels(): void {
        $levels = [
            new Level(),
            new Level(),
            new Level()
        ];
        $useCaseInput = new GetLevelsUseCaseInput();
        $useCaseOutput = new GetLevelsUseCaseOutput();

        $this->entityGateway->method('execute')->with($this->equalTo(null), $this->equalTo(null))->willReturn(
            $levels
        );
        $this->useCase->execute($useCaseInput, $useCaseOutput);

        $actual = $useCaseOutput->getLevels();
        $expected = $levels;

        $this->assertEquals($expected, $actual);
    }
}