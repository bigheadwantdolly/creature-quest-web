<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 12:19 AM
 */

namespace App\Tests\UseCase;

use App\Entity\Color;
use App\EntityGateway\IGetManyColorGateway;
use App\UseCase\GetColorsUseCase;
use App\UseCase\GetColorsUseCaseInput;
use App\UseCase\GetColorsUseCaseOutput;
use App\UseCase\IGetColorsUseCase;
use App\UseCase\IGetColorsUseCaseInput;
use App\UseCase\IGetColorsUseCaseOutput;
use PHPUnit\Framework\TestCase;

class GetColorsUseCaseTest extends TestCase {

    /**
     * @var Color[]
     */
    private $colors;

    /**
     * @var IGetColorsUseCase
     */
    private $useCase;

    /**
     * @var IGetColorsUseCaseInput
     */
    private $useCaseInput;

    /**
     * @var IGetColorsUseCaseOutput
     */
    private $useCaseOutput;

    protected function setUp() {
        $this->colors = [
            new Color(),
            new Color(),
            new Color()
        ];
        $entityGateway = $this->createMock(IGetManyColorGateway::class);
        $entityGateway->method('execute')->willReturn($this->colors);

        $this->useCase = new GetColorsUseCase($entityGateway);
        $this->useCaseInput = new GetColorsUseCaseInput();
        $this->useCaseOutput = new GetColorsUseCaseOutput();
    }

    protected function tearDown() {
        $this->useCase = null;
    }

    public function testOutputsColors(): void {
        $this->useCase->execute($this->useCaseInput, $this->useCaseOutput);

        $actual = $this->useCaseOutput->getColors();
        $expected = $this->colors;

        $this->assertEquals($expected, $actual);
    }
}