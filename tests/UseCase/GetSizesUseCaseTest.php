<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 12:19 AM
 */

namespace App\Tests\UseCase;

use App\Entity\Size;
use App\EntityGateway\IGetManySizeGateway;
use App\UseCase\GetSizesUseCase;
use App\UseCase\GetSizesUseCaseInput;
use App\UseCase\GetSizesUseCaseOutput;
use App\UseCase\IGetSizesUseCase;
use App\UseCase\IGetSizesUseCaseInput;
use App\UseCase\IGetSizesUseCaseOutput;
use PHPUnit\Framework\TestCase;

class GetSizesUseCaseTest extends TestCase {

    /**
     * @var Size[]
     */
    private $sizes;

    /**
     * @var IGetSizesUseCase
     */
    private $useCase;

    /**
     * @var IGetSizesUseCaseInput
     */
    private $useCaseInput;

    /**
     * @var IGetSizesUseCaseOutput
     */
    private $useCaseOutput;

    protected function setUp() {
        $this->sizes = [
            new Size(),
            new Size(),
            new Size()
        ];
        $entityGateway = $this->createMock(IGetManySizeGateway::class);
        $entityGateway->method('execute')->willReturn($this->sizes);

        $this->useCase = new GetSizesUseCase($entityGateway);
        $this->useCaseInput = new GetSizesUseCaseInput();
        $this->useCaseOutput = new GetSizesUseCaseOutput();
    }

    protected function tearDown() {
        $this->useCase = null;
    }

    public function testOutputsSizes(): void {
        $this->useCase->execute($this->useCaseInput, $this->useCaseOutput);

        $actual = $this->useCaseOutput->getSizes();
        $expected = $this->sizes;

        $this->assertEquals($expected, $actual);
    }
}