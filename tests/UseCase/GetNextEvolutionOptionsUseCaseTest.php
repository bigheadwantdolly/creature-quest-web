<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 7/7/2018
 * Time: 2:46 PM
 */

namespace App\Tests\UseCase;

use App\Entity\Creature;
use App\Entity\Evolution;
use App\EntityGateway\IGetNextEvolutionGateway;
use App\EntityGateway\IGetNextEvolutionOptionsGateway;
use App\UseCase\GetNextEvolutionOptionsUseCase;
use App\UseCase\GetNextEvolutionOptionsUseCaseInput;
use App\UseCase\GetNextEvolutionOptionsUseCaseOutput;
use App\UseCase\IGetNextEvolutionOptionsUseCase;
use App\UseCase\IGetNextEvolutionOptionsUseCaseInput;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetNextEvolutionOptionsUseCaseTest extends TestCase {

    /**
     * @var Creature[]
     */
    private $entities;
    /**
     * @var Evolution
     */
    private $evolution;
    /**
     * @var IGetNextEvolutionGateway|MockObject
     */
    private $getNextEvolutionGateway;
    /**
     * @var IGetNextEvolutionOptionsGateway|MockObject
     */
    private $getNextEvolutionOptionsGateway;
    /**
     * @var IGetNextEvolutionOptionsUseCase
     */
    private $useCase;
    /**
     * @var IGetNextEvolutionOptionsUseCaseInput
     */
    private $useCaseInput;

    protected function setUp() {
        $this->entities = [
            new Creature(),
            new Creature(),
            new Creature()
        ];
        $this->getNextEvolutionGateway = $this->createMock(IGetNextEvolutionGateway::class);
        $this->getNextEvolutionOptionsGateway = $this->createMock(IGetNextEvolutionOptionsGateway::class);

        $this->useCase = new GetNextEvolutionOptionsUseCase(
            $this->getNextEvolutionGateway,
            $this->getNextEvolutionOptionsGateway
        );
        $this->useCaseInput = new GetNextEvolutionOptionsUseCaseInput();

        $this->useCaseInput->setColorId('12345');
        $this->useCaseInput->setEvolutionId('23456');
        $this->useCaseInput->setSizeId('34567');

        $this->evolution = new Evolution();

        $this->evolution->setId('12345');
        $this->evolution->setCardinality(2);
    }

    protected function tearDown() {
        $this->entities = null;
        $this->getNextEvolutionOptionsGateway = null;
        $this->useCase = null;
        $this->useCaseInput = null;
    }

    public function testGetNextEvolutionGatewayIsCalled(): void {
        $useCaseOutput = new GetNextEvolutionOptionsUseCaseOutput();

        $this->getNextEvolutionGateway->expects($this->once())->method('execute')->with(
            $this->useCaseInput->getEvolutionId()
        );

        $this->useCase->execute($this->useCaseInput, $useCaseOutput);
    }

    public function testGetNextEvolutionOptionsGatewayIsCalled(): void {
        $useCaseOutput = new GetNextEvolutionOptionsUseCaseOutput();

        $this->getNextEvolutionGateway->method('execute')->willReturn($this->evolution);
        $this->getNextEvolutionOptionsGateway->expects($this->once())->method('execute')->withAnyParameters();

        $this->useCase->execute($this->useCaseInput, $useCaseOutput);
    }

    public function testGetNextEvolutionOptionsGatewayIsNotCalledWhenNoNextEvolutionIsReturned(): void {
        $useCaseOutput = new GetNextEvolutionOptionsUseCaseOutput();

        $this->getNextEvolutionGateway->method('execute')->willReturn(null);
        $this->getNextEvolutionOptionsGateway->expects($this->never())->method('execute')->withAnyParameters();

        $this->useCase->execute($this->useCaseInput, $useCaseOutput);
    }

    public function testUseCaseInputDataPassedIntoGetNextEvolutionOptionsGateway(): void {
        $useCaseOutput = new GetNextEvolutionOptionsUseCaseOutput();

        $this->getNextEvolutionGateway->method('execute')->willReturn($this->evolution);
        $this->getNextEvolutionOptionsGateway->expects($this->once())->method('execute')->with(
            $this->useCaseInput->getColorId(),
            $this->evolution->getId(),
            $this->useCaseInput->getSizeId()
        );

        $this->useCase->execute($this->useCaseInput, $useCaseOutput);
    }

    public function testUseCaseOutputIsPopulated(): void {
        $useCaseOutput = new GetNextEvolutionOptionsUseCaseOutput();

        $this->getNextEvolutionGateway->method('execute')->willReturn($this->evolution);
        $this->getNextEvolutionOptionsGateway->method('execute')->willReturn($this->entities);

        $this->useCase->execute($this->useCaseInput, $useCaseOutput);

        $actual = $useCaseOutput->getCreatures();
        $expected = $this->entities;

        $this->assertEquals($expected, $actual);
    }

}