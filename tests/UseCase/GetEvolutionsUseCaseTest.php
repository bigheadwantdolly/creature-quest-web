<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 12:19 AM
 */

namespace App\Tests\UseCase;

use App\Entity\Evolution;
use App\EntityGateway\IGetManyEvolutionGateway;
use App\UseCase\GetEvolutionsUseCase;
use App\UseCase\GetEvolutionsUseCaseInput;
use App\UseCase\GetEvolutionsUseCaseOutput;
use App\UseCase\IGetEvolutionsUseCase;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetEvolutionsUseCaseTest extends TestCase {

    /**
     * @var MockObject
     */
    private $entityGateway;
    /**
     * @var IGetEvolutionsUseCase
     */
    private $useCase;

    protected function setUp() {
        $this->entityGateway = $this->createMock(IGetManyEvolutionGateway::class);
        $this->useCase = new GetEvolutionsUseCase($this->entityGateway);
    }

    protected function tearDown() {
        $this->entityGateway = null;
        $this->useCase = null;
    }

    public function testFiltersByRarity(): void {
        $evolutions = [
            new Evolution()
        ];
        $rarityId = '1';
        $useCaseInput = new GetEvolutionsUseCaseInput();
        $useCaseOutput = new GetEvolutionsUseCaseOutput();

        $this->entityGateway->method('execute')->with($this->equalTo($rarityId), $this->equalTo(null))->willReturn(
            $evolutions
        );
        $useCaseInput->setRarityId($rarityId);
        $this->useCase->execute($useCaseInput, $useCaseOutput);

        $actual = count($useCaseOutput->getEvolutions());
        $expected = 1;

        $this->assertEquals($expected, $actual);
    }

    public function testFiltersBySize(): void {
        $evolutions = [
            new Evolution()
        ];
        $sizeId = '1';
        $useCaseInput = new GetEvolutionsUseCaseInput();
        $useCaseOutput = new GetEvolutionsUseCaseOutput();

        $this->entityGateway->method('execute')->with($this->equalTo(null), $this->equalTo($sizeId))->willReturn(
            $evolutions
        );
        $useCaseInput->setSizeId($sizeId);
        $this->useCase->execute($useCaseInput, $useCaseOutput);

        $actual = count($useCaseOutput->getEvolutions());
        $expected = 1;

        $this->assertEquals($expected, $actual);
    }

    public function testOutputsEvolutions(): void {
        $evolutions = [
            new Evolution(),
            new Evolution(),
            new Evolution()
        ];
        $useCaseInput = new GetEvolutionsUseCaseInput();
        $useCaseOutput = new GetEvolutionsUseCaseOutput();

        $this->entityGateway->method('execute')->with($this->equalTo(null), $this->equalTo(null))->willReturn(
            $evolutions
        );
        $this->useCase->execute($useCaseInput, $useCaseOutput);

        $actual = $useCaseOutput->getEvolutions();
        $expected = $evolutions;

        $this->assertEquals($expected, $actual);
    }
}