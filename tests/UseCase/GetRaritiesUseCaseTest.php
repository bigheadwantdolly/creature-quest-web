<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 12:19 AM
 */

namespace App\Tests\UseCase;

use App\Entity\Rarity;
use App\EntityGateway\IGetManyRarityGateway;
use App\UseCase\GetRaritiesUseCase;
use App\UseCase\GetRaritiesUseCaseInput;
use App\UseCase\GetRaritiesUseCaseOutput;
use App\UseCase\IGetRaritiesUseCase;
use App\UseCase\IGetRaritiesUseCaseInput;
use App\UseCase\IGetRaritiesUseCaseOutput;
use PHPUnit\Framework\TestCase;

class GetRaritiesUseCaseTest extends TestCase {

    /**
     * @var Rarity[]
     */
    private $rarities;

    /**
     * @var IGetRaritiesUseCase
     */
    private $useCase;

    /**
     * @var IGetRaritiesUseCaseInput
     */
    private $useCaseInput;

    /**
     * @var IGetRaritiesUseCaseOutput
     */
    private $useCaseOutput;

    protected function setUp() {
        $this->rarities = [
            new Rarity(),
            new Rarity(),
            new Rarity()
        ];
        $entityGateway = $this->createMock(IGetManyRarityGateway::class);
        $entityGateway->method('execute')->willReturn($this->rarities);

        $this->useCase = new GetRaritiesUseCase($entityGateway);
        $this->useCaseInput = new GetRaritiesUseCaseInput();
        $this->useCaseOutput = new GetRaritiesUseCaseOutput();
    }

    protected function tearDown() {
        $this->useCase = null;
    }

    public function testOutputsRarities(): void {
        $this->useCase->execute($this->useCaseInput, $this->useCaseOutput);

        $actual = $this->useCaseOutput->getRarities();
        $expected = $this->rarities;

        $this->assertEquals($expected, $actual);
    }
}