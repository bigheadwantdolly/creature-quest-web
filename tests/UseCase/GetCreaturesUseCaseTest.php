<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 12:19 AM
 */

namespace App\Tests\UseCase;

use App\Entity\Creature;
use App\EntityGateway\IGetManyCreatureGateway;
use App\UseCase\GetCreaturesUseCase;
use App\UseCase\GetCreaturesUseCaseInput;
use App\UseCase\GetCreaturesUseCaseOutput;
use App\UseCase\IGetCreaturesUseCase;
use App\UseCase\IGetCreaturesUseCaseInput;
use App\UseCase\IGetCreaturesUseCaseOutput;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class GetCreaturesUseCaseTest extends TestCase {

    /**
     * @var Creature[]
     */
    private $creatures;
    /**
     * @var MockObject
     */
    private $entityGateway;
    /**
     * @var IGetCreaturesUseCase
     */
    private $useCase;
    /**
     * @var IGetCreaturesUseCaseInput
     */
    private $useCaseInput;
    /**
     * @var IGetCreaturesUseCaseOutput
     */
    private $useCaseOutput;

    protected function setUp() {
        $this->creatures = [
            new Creature(),
            new Creature(),
            new Creature()
        ];
        $this->entityGateway = $this->createMock(IGetManyCreatureGateway::class);

        $this->useCase = new GetCreaturesUseCase($this->entityGateway);
        $this->useCaseInput = new GetCreaturesUseCaseInput();
        $this->useCaseOutput = new GetCreaturesUseCaseOutput();
    }

    protected function tearDown() {
        $this->useCase = null;
    }

    public function testFiltersByColor(): void {
        $creatures = [
            new Creature()
        ];
        $colorId = '1';
        $this->entityGateway->method('execute')->with($this->equalTo([$colorId]))->willReturn($creatures);
        $this->useCaseInput->setColorId($colorId);
        $this->useCase->execute($this->useCaseInput, $this->useCaseOutput);

        $actual = count($this->useCaseOutput->getCreatures());
        $expected = 1;

        $this->assertEquals($expected, $actual);
    }

    public function testOutputsCreatures(): void {
        $this->entityGateway->method('execute')->willReturn($this->creatures);
        $this->useCase->execute($this->useCaseInput, $this->useCaseOutput);

        $actual = $this->useCaseOutput->getCreatures();
        $expected = $this->creatures;

        $this->assertEquals($expected, $actual);
    }
}