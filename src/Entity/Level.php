<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:22 PM
 */

namespace App\Entity;

class Level implements IEntity {

    /**
     * @var string
     */
    private $id;
    /**
     * @var LevelRequirement[]
     */
    private $levelRequirements;
    /**
     * @var int
     */
    private $number;

    /**
     * @return string
     */
    public function getId(): ?string {
        return $this->id;
    }

    /**
     * @return LevelRequirement[]
     */
    public function getLevelRequirements(): array {
        return $this->levelRequirements;
    }

    /**
     * @return int
     */
    public function getNumber(): int {
        return $this->number;
    }

    /**
     * @param string $id
     */
    public function setId(?string $id) {
        $this->id = $id;
    }

    /**
     * @param LevelRequirement[] $levelRequirements
     */
    public function setLevelRequirements(array $levelRequirements) {
        $this->levelRequirements = $levelRequirements;
    }

    /**
     * @param int $number
     */
    public function setNumber(int $number) {
        $this->number = $number;
    }

}