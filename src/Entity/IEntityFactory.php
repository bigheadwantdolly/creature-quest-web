<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 2:04 PM
 */

namespace App\Entity;

interface IEntityFactory {

    function create(string $className): IEntity;
}