<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/27/2018
 * Time: 11:03 AM
 */

namespace App\Entity;

class LevelRequirement implements IEntity {
    /**
     * @var Evolution
     */
    private $evolution;
    /**
     * @var string
     */
    private $id;
    /**
     * @var Size
     */
    private $size;

    /**
     * @return Evolution
     */
    public function getEvolution(): ?Evolution {
        return $this->evolution;
    }

    /**
     * @return string
     */
    public function getId(): ?string {
        return $this->id;
    }

    /**
     * @return Size
     */
    public function getSize(): ?Size {
        return $this->size;
    }

    /**
     * @param Evolution $evolution
     */
    public function setEvolution(?Evolution $evolution) {
        $this->evolution = $evolution;
    }

    /**
     * @param string $id
     */
    public function setId(?string $id) {
        $this->id = $id;
    }

    /**
     * @param Size $size
     */
    public function setSize(?Size $size) {
        $this->size = $size;
    }
}