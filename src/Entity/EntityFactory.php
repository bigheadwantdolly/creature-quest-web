<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 2:03 PM
 */

namespace App\Entity;

use InvalidArgumentException;

class EntityFactory implements IEntityFactory {

    public function create(string $className): IEntity {
        $entity = new $className();

        if (!$entity instanceof IEntity) {
            throw new InvalidArgumentException(sprintf('expected instance of `IEntity` but received `%s`', $className));
        }

        return $entity;
    }
}