<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/31/18
 * Time: 2:02 PM
 */

namespace App\ApiRequest;

use Exception;

class ApiUpdateRequest extends ApiRequest implements IApiUpdateRequest {

    /**
     * @param IApiUpdateRequestConfiguration $apiRequestConfiguration
     *
     * @return array
     * @throws Exception
     */
    function execute(IApiUpdateRequestConfiguration $apiRequestConfiguration): array {
        $curl = curl_init();
        $url = $apiRequestConfiguration->getEndpointUrl();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($curl, CURLOPT_POSTFIELDS, $apiRequestConfiguration->getParameters());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($curl);
        curl_close($curl);

        $decodedData = json_decode($data, true);

        if (isset($decodedData[self::API_REQUEST_KEY_DATA])) {
            $decodedData = $decodedData[self::API_REQUEST_KEY_DATA];
        } else {
            throw new Exception(implode(PHP_EOL, $decodedData[self::API_REQUEST_KEY_ERRORS]));
        }

        return $decodedData;
    }

}