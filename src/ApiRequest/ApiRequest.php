<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/31/18
 * Time: 2:02 PM
 */

namespace App\ApiRequest;

use Psr\Log\LoggerInterface;

abstract class ApiRequest {

    public const API_REQUEST_KEY_DATA = 'data';
    public const API_REQUEST_KEY_ERRORS = 'errors';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
    }

    protected function getApiAddress(): string {
        return 'http://cq-api:8080/';
    }

    private function implodeParameter($key, $value): string {
        $result = '';

        if (is_array($value)) {
            $init = false;

            foreach ($value as $item) {
                $prefix = '&';

                if ($init == false) {
                    $prefix = '';
                    $init = true;
                }

                $result .= $prefix . $key . '=' . $item;
            }
        } else {
            $result = $key . '=' . $value;
        }

        return $result;
    }

    protected function implodeParameters(array $parameters): string {
        $result = '';
        $init = false;

        if ($parameters != null) {
            foreach ($parameters as $key => $value) {
                $prefix = '&';

                if ($init == false) {
                    $prefix = '?';
                    $init = true;
                }

                $result .= $prefix . $this->implodeParameter($key, $value);
            }
        }

        return $result;
    }
}