<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 12:13 PM
 */

namespace App\ApiRequest;

interface IApiRequestConfigurationFactory {

    function createCreateConfiguration(
        string $endpointUrlSuffix,
        array $parameters = []
    ): IApiCreateRequestConfiguration;

    function createDeleteConfiguration(
        string $endpointUrlSuffix,
        array $parameters = []
    ): IApiDeleteRequestConfiguration;

    function createReadConfiguration(
        string $endpointUrlSuffix,
        array $parameters = [],
        bool $expectsSingleResult = false
    ): IApiReadRequestConfiguration;

    function createUpdateConfiguration(
        string $endpointUrlSuffix,
        array $parameters = []
    ): IApiUpdateRequestConfiguration;
}