<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 1:44 PM
 */

namespace App\ApiRequest;

interface IApiCreateRequest {

    function execute(IApiCreateRequestConfiguration $apiRequestConfiguration): array;
}