<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 12:12 PM
 */

namespace App\ApiRequest;

class ApiRequestConfigurationFactory implements IApiRequestConfigurationFactory {

    function createCreateConfiguration(
        string $endpointUrlSuffix,
        array $parameters = []
    ): IApiCreateRequestConfiguration {
        $configuration = new ApiCreateRequestConfiguration($endpointUrlSuffix, $parameters);

        return $configuration;
    }

    function createDeleteConfiguration(
        string $endpointUrlSuffix,
        array $parameters = []
    ): IApiDeleteRequestConfiguration {
        $configuration = new ApiDeleteRequestConfiguration($endpointUrlSuffix, $parameters);

        return $configuration;
    }

    function createReadConfiguration(
        string $endpointUrlSuffix,
        array $parameters = [],
        bool $expectsSingleResult = false
    ): IApiReadRequestConfiguration {
        $configuration = new ApiReadRequestConfiguration(
            $endpointUrlSuffix,
            $parameters,
            $expectsSingleResult
        );

        return $configuration;
    }

    function createUpdateConfiguration(
        string $endpointUrlSuffix,
        array $parameters = []
    ): IApiUpdateRequestConfiguration {
        $configuration = new ApiUpdateRequestConfiguration($endpointUrlSuffix, $parameters);

        return $configuration;
    }
}