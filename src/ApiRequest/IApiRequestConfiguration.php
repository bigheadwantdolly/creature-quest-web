<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 12:26 PM
 */

namespace App\ApiRequest;

interface IApiRequestConfiguration {

    function getEndpointUrl(): string;

    function getEndpointUrlSuffix(): string;

    function getParameters(): array;

}