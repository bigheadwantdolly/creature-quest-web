<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 5:14 PM
 */

namespace App\ApiRequest;

interface IApiReadRequest {

    function execute(IApiReadRequestConfiguration $apiRequestConfiguration): array;
}