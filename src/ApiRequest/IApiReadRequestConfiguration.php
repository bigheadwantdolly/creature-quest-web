<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 12:15 PM
 */

namespace App\ApiRequest;

interface IApiReadRequestConfiguration extends IApiRequestConfiguration {

    function getExpectsSingleResult(): bool;

}