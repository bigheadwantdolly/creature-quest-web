<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 12:23 PM
 */

namespace App\ApiRequest;

class ApiReadRequestConfiguration extends ApiRequestConfiguration implements IApiReadRequestConfiguration {

    /**
     * @var bool
     */
    private $expectsSingleResult;

    public function __construct(
        string $endpointUrlSuffix = '',
        array $parameters = [],
        bool $expectsSingleResult = false
    ) {
        parent::__construct($endpointUrlSuffix, $parameters);

        $this->expectsSingleResult = $expectsSingleResult;
    }

    function getExpectsSingleResult(): bool {
        return $this->expectsSingleResult;
    }
}