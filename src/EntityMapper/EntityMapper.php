<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/31/18
 * Time: 3:18 PM
 */

namespace App\EntityMapper;

use App\Entity\IEntity;
use App\Entity\IEntityFactory;

abstract class EntityMapper implements IEntityMapper {

    static $SourceKeyId = 'id';

    /**
     * @var IEntityFactory
     */
    private $entityFactory;

    public function __construct(IEntityFactory $entityFactory) {
        $this->entityFactory = $entityFactory;
    }

    abstract protected function createEntity(): IEntity;

    protected function getEntityFactory(): IEntityFactory {
        return $this->entityFactory;
    }

    /**
     * @param array[]   $input
     * @param IEntity[] $output
     */
    public function mapMany(array $input, array &$output): void {
        foreach ($input as $item) {
            $entity = $this->createEntity();

            $this->mapOne($item, $entity);

            $output[] = $entity;
        }
    }
}