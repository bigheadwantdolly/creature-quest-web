<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/31/18
 * Time: 2:13 PM
 */

namespace App\EntityMapper;

use App\Entity\Evolution;
use App\Entity\IEntity;
use App\Entity\IEntityFactory;
use App\Entity\LevelRequirement;
use App\Entity\Size;
use App\Util\ArrayHelper;
use InvalidArgumentException;

class LevelRequirementMapper extends EntityMapper {

    static $EntityClassName = LevelRequirement::class;
    static $SourceKeyEvolution = 'evolution';
    static $SourceKeySize = 'size';

    /**
     * @var EvolutionMapper
     */
    private $evolutionMapper;
    /**
     * @var SizeMapper
     */
    private $sizeMapper;

    function __construct(
        IEntityFactory $entityFactory,
        EvolutionMapper $evolutionMapper,
        SizeMapper $sizeMapper
    ) {
        parent::__construct($entityFactory);

        $this->evolutionMapper = $evolutionMapper;
        $this->sizeMapper = $sizeMapper;
    }

    function createEntity(): IEntity {
        return $this->getEntityFactory()->create(self::$EntityClassName);
    }

    private function mapEvolution(array $input, LevelRequirement &$output): void {
        $entity = $this->getEntityFactory()->create(Evolution::class);

        if (isset($input[self::$SourceKeyEvolution]) && $input[self::$SourceKeyEvolution] != null) {
            try {
                $this->evolutionMapper->mapOne($input[self::$SourceKeyEvolution], $entity);
            } catch (InvalidArgumentException $e) {
                $entity = null;
            }
        }

        if ($entity instanceof Evolution) {
            $output->setEvolution($entity);
        }
    }

    /**
     * @param string[] $input
     * @param IEntity  $output
     *
     * @throws InvalidArgumentException
     */
    function mapOne(array $input, IEntity &$output): void {
        $id = ArrayHelper::getValue($input, self::$SourceKeyId);

        if ($output instanceof LevelRequirement) {
            $output->setId($id);

            $this->mapEvolution($input, $output);
            $this->mapSize($input, $output);
        } else {
            throw new InvalidArgumentException(
                sprintf('expected output type `%s` but received `%s`', self::$EntityClassName, get_class($output))
            );
        }
    }

    private function mapSize(array $input, LevelRequirement &$output): void {
        $entity = $this->getEntityFactory()->create(Size::class);

        if (isset($input[self::$SourceKeySize]) && $input[self::$SourceKeySize] != null) {
            try {
                $this->sizeMapper->mapOne($input[self::$SourceKeySize], $entity);
            } catch (InvalidArgumentException $e) {
                $entity = null;
            }
        }

        if ($entity instanceof Size) {
            $output->setSize($entity);
        }
    }
}