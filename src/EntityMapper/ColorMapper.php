<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/31/18
 * Time: 2:13 PM
 */

namespace App\EntityMapper;

use App\Entity\Color;
use App\Entity\IEntity;
use App\Util\ArrayHelper;
use InvalidArgumentException;

class ColorMapper extends EntityMapper {

    static $EntityClassName = Color::class;
    static $SourceKeyName = 'name';

    function createEntity(): IEntity {
        return $this->getEntityFactory()->create(self::$EntityClassName);
    }

    /**
     * @param string[] $input
     * @param IEntity  $output
     *
     * @throws InvalidArgumentException
     */
    function mapOne(array $input, IEntity &$output): void {
        $id = ArrayHelper::getValue($input, self::$SourceKeyId);
        $name = ArrayHelper::getValue($input, self::$SourceKeyName);

        if ($output instanceof Color) {
            $output->setId($id);
            $output->setName($name);
        } else {
            throw new InvalidArgumentException(
                sprintf('expected output type `%s` but received `%s`', self::$EntityClassName, get_class($output))
            );
        }
    }
}