<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/31/18
 * Time: 2:13 PM
 */

namespace App\EntityMapper;

use App\Entity\IEntity;
use App\Entity\IEntityFactory;
use App\Entity\Level;
use App\Util\ArrayHelper;
use InvalidArgumentException;

class LevelMapper extends EntityMapper {

    static $EntityClassName = Level::class;
    static $SourceKeyLevelRequirements = 'levelRequirements';
    static $SourceKeyNumber = 'number';

    /**
     * @var LevelRequirementMapper
     */
    private $levelRequirementMapper;

    function __construct(
        IEntityFactory $entityFactory,
        LevelRequirementMapper $levelRequirementMapper
    ) {
        parent::__construct($entityFactory);

        $this->levelRequirementMapper = $levelRequirementMapper;
    }

    function createEntity(): IEntity {
        return $this->getEntityFactory()->create(self::$EntityClassName);
    }

    private function mapLevelRequirements(array $input, Level &$output): void {
        $entities = [];

        if (isset($input[self::$SourceKeyLevelRequirements]) && $input[self::$SourceKeyLevelRequirements] != null) {
            try {
                $this->levelRequirementMapper->mapMany($input[self::$SourceKeyLevelRequirements], $entities);
            } catch (InvalidArgumentException $e) {
                // do nothing
            }
        }

        $output->setLevelRequirements($entities);
    }

    /**
     * @param string[] $input
     * @param IEntity  $output
     *
     * @throws InvalidArgumentException
     */
    function mapOne(array $input, IEntity &$output): void {
        $id = ArrayHelper::getValue($input, self::$SourceKeyId);
        $number = ArrayHelper::getValue($input, self::$SourceKeyNumber);

        if ($output instanceof Level) {
            $output->setId($id);
            $output->setNumber($number);

            $this->mapLevelRequirements($input, $output);
        } else {
            throw new InvalidArgumentException(
                sprintf('expected output type `%s` but received `%s`', self::$EntityClassName, get_class($output))
            );
        }
    }
}