<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/31/18
 * Time: 2:13 PM
 */

namespace App\EntityMapper;

use App\Entity\Evolution;
use App\Entity\IEntity;
use App\Entity\IEntityFactory;
use App\Util\ArrayHelper;
use InvalidArgumentException;

class EvolutionMapper extends EntityMapper {

    static $EntityClassName = Evolution::class;
    static $SourceKeyCardinality = 'cardinality';
    static $SourceKeyRarities = 'rarities';
    static $SourceKeySizes = 'sizes';

    /**
     * @var RarityMapper
     */
    private $rarityMapper;

    /**
     * @var SizeMapper
     */
    private $sizeMapper;

    function __construct(
        IEntityFactory $entityFactory,
        RarityMapper $rarityMapper,
        SizeMapper $sizeMapper
    ) {
        parent::__construct($entityFactory);

        $this->rarityMapper = $rarityMapper;
        $this->sizeMapper = $sizeMapper;
    }

    function createEntity(): IEntity {
        return $this->getEntityFactory()->create(self::$EntityClassName);
    }

    /**
     * @param string[] $input
     * @param IEntity  $output
     *
     * @throws InvalidArgumentException
     */
    function mapOne(array $input, IEntity &$output): void {
        $id = ArrayHelper::getValue($input, self::$SourceKeyId);
        $cardinality = ArrayHelper::getValue($input, self::$SourceKeyCardinality);

        if ($output instanceof Evolution) {
            $output->setId($id);
            $output->setCardinality($cardinality);

            $this->mapRarities($input, $output);
            $this->mapSizes($input, $output);
        } else {
            throw new InvalidArgumentException(
                sprintf('expected output type `%s` but received `%s`', self::$EntityClassName, get_class($output))
            );
        }
    }

    private function mapRarities(array $input, Evolution &$output): void {
        $entities = [];

        if (isset($input[self::$SourceKeyRarities]) && $input[self::$SourceKeyRarities] != null) {
            try {
                $this->rarityMapper->mapMany($input[self::$SourceKeyRarities], $entities);
            } catch (InvalidArgumentException $e) {
                // do nothing
            }
        }

        $output->setRarities($entities);
    }

    private function mapSizes(array $input, Evolution &$output): void {
        $entities = [];

        if (isset($input[self::$SourceKeySizes]) && $input[self::$SourceKeySizes] != null) {
            try {
                $this->sizeMapper->mapMany($input[self::$SourceKeySizes], $entities);
            } catch (InvalidArgumentException $e) {
                // do nothing
            }
        }

        $output->setSizes($entities);
    }
}