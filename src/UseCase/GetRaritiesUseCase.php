<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

use App\EntityGateway\IGetManyRarityGateway;

class GetRaritiesUseCase implements IGetRaritiesUseCase {

    /**
     * @var IGetManyRarityGateway
     */
    private $getAllRaritiesEntityGateway;

    public function __construct(IGetManyRarityGateway $getAllRaritiesEntityGateway) {
        $this->getAllRaritiesEntityGateway = $getAllRaritiesEntityGateway;
    }

    public function execute(IGetRaritiesUseCaseInput $input, IGetRaritiesUseCaseOutput $output): void {
        $rarities = $this->getAllRaritiesEntityGateway->execute();

        $output->setRarities($rarities);
    }
}