<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 12:54 AM
 */

namespace App\UseCase;

use App\EntityGateway\IGetNextEvolutionGateway;
use App\EntityGateway\IGetNextEvolutionOptionsGateway;

class GetNextEvolutionOptionsUseCase implements IGetNextEvolutionOptionsUseCase {
    /**
     * @var IGetNextEvolutionGateway
     */
    private $getNextEvolutionGateway;

    /**
     * @var IGetNextEvolutionOptionsGateway
     */
    private $getNextEvolutionOptionsGateway;

    public function __construct(
        IGetNextEvolutionGateway $getNextEvolutionGateway,
        IGetNextEvolutionOptionsGateway $getNextEvolutionOptionsGateway
    ) {
        $this->getNextEvolutionOptionsGateway = $getNextEvolutionOptionsGateway;
        $this->getNextEvolutionGateway = $getNextEvolutionGateway;
    }

    public function execute(
        IGetNextEvolutionOptionsUseCaseInput $input,
        IGetNextEvolutionOptionsUseCaseOutput $output
    ): void {
        $nextEvolution = $this->getNextEvolutionGateway->execute($input->getEvolutionId());
        $creatures = [];

        if ($nextEvolution !== null) {
            $creatures = $this->getNextEvolutionOptionsGateway->execute(
                $input->getColorId(),
                $nextEvolution->getId(),
                $input->getSizeId()
            );
        }

        $output->setCreatures($creatures);
    }
}