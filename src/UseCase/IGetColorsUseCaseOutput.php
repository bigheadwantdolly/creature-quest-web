<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:16 PM
 */

namespace App\UseCase;

use App\Entity\Color;

interface IGetColorsUseCaseOutput {

    /**
     * @return Color[]
     */
    function getColors(): array;

    /**
     * @param Color[] $colors
     */
    function setColors(array $colors): void;

}