<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:16 PM
 */

namespace App\UseCase;

interface IGetLevelsUseCase {
    function execute(IGetLevelsUseCaseInput $input, IGetLevelsUseCaseOutput $output): void;
}