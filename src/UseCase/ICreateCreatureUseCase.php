<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/10/2018
 * Time: 4:10 PM
 */

namespace App\UseCase;

interface ICreateCreatureUseCase {
    function execute(ICreateCreatureUseCaseInput $input, ICreateCreatureUseCaseOutput $output): void;
}