<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

use App\Entity\Level;

class GetLevelsUseCaseOutput implements IGetLevelsUseCaseOutput {

    /**
     * @var Level[]
     */
    private $levels;

    public function __construct() {
        $this->levels = [];
    }

    /**
     * @return Level[]
     */
    function getLevels(): array {
        return $this->levels;
    }

    /**
     * @param Level[] $levels
     */
    function setLevels(array $levels): void {
        $this->levels = $levels;
    }
}