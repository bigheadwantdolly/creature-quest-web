<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 12:57 AM
 */

namespace App\UseCase;

interface IGetCreaturesUseCaseInput {

    function getColorId(): string;

    function setColorId(string $colorId): void;

}