<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

use App\EntityGateway\IGetManyAttackGateway;

class GetAttacksUseCase implements IGetAttacksUseCase {

    /**
     * @var IGetManyAttackGateway
     */
    private $getAllAttacksEntityGateway;

    public function __construct(IGetManyAttackGateway $getAllAttacksEntityGateway) {
        $this->getAllAttacksEntityGateway = $getAllAttacksEntityGateway;
    }

    public function execute(IGetAttacksUseCaseInput $input, IGetAttacksUseCaseOutput $output): void {
        $attacks = $this->getAllAttacksEntityGateway->execute();

        $output->setAttacks($attacks);
    }
}