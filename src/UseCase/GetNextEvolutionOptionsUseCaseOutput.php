<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 1:10 AM
 */

namespace App\UseCase;

use App\Entity\Creature;

class GetNextEvolutionOptionsUseCaseOutput implements IGetNextEvolutionOptionsUseCaseOutput {

    /**
     * @var Creature[]
     */
    private $creatures;

    public function __construct() {
        $this->creatures = [];
    }

    /**
     * @return Creature[]
     */
    function getCreatures(): array {
        return $this->creatures;
    }

    /**
     * @param Creature[] $creatures
     */
    function setCreatures(array $creatures): void {
        $this->creatures = $creatures;
    }
}