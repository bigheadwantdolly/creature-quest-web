<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:16 PM
 */

namespace App\UseCase;

interface IGetLevelsUseCaseInput {

    /**
     * Returns GUID of the evolution to use to filter the data set
     *
     * @return null|string
     */
    public function getEvolutionId(): ?string;

    /**
     * Returns GUID of the size to use to filter the data set
     *
     * @return null|string
     */
    public function getSizeId(): ?string;

    /**
     * Sets the GUID of the evolution to use to filter the data set
     *
     * @param null|string $evolutionId
     */
    public function setEvolutionId(?string $evolutionId): void;

    /**
     * Sets the GUID of the size to use to filter the data set
     *
     * @param null|string $sizeId
     */
    public function setSizeId(?string $sizeId): void;

}