<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

use App\Entity\Color;

class GetColorsUseCaseOutput implements IGetColorsUseCaseOutput {

    /**
     * @var Color[]
     */
    private $colors;

    public function __construct() {
        $this->colors = [];
    }

    /**
     * @return Color[]
     */
    function getColors(): array {
        return $this->colors;
    }

    /**
     * @param Color[] $colors
     */
    function setColors(array $colors): void {
        $this->colors = $colors;
    }
}