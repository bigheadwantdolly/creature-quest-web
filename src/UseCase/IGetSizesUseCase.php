<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:16 PM
 */

namespace App\UseCase;

interface IGetSizesUseCase {
    function execute(IGetSizesUseCaseInput $input, IGetSizesUseCaseOutput $output): void;
}