<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/10/2018
 * Time: 4:11 PM
 */

namespace App\UseCase;

use App\Entity\Creature;
use App\EntityGateway\ICreateCreatureGateway;
use App\EntityGateway\IGetOneAttackGateway;
use App\EntityGateway\IGetOneColorGateway;
use App\EntityGateway\IGetOneCreatureGateway;
use App\EntityGateway\IGetOneEvolutionGateway;
use App\EntityGateway\IGetOneLevelGateway;
use App\EntityGateway\IGetOneRarityGateway;
use App\EntityGateway\IGetOneSizeGateway;

class CreateCreatureUseCase implements ICreateCreatureUseCase {

    /**
     * @var ICreateCreatureGateway
     */
    private $createEntityGateway;
    /**
     * @var IGetOneAttackGateway
     */
    private $getOneAttackGateway;
    /**
     * @var IGetOneColorGateway
     */
    private $getOneColorGateway;
    /**
     * @var IGetOneCreatureGateway
     */
    private $getOneCreatureGateway;
    /**
     * @var IGetOneEvolutionGateway
     */
    private $getOneEvolutionGateway;
    /**
     * @var IGetOneLevelGateway
     */
    private $getOneLevelGateway;
    /**
     * @var IGetOneRarityGateway
     */
    private $getOneRarityGateway;
    /**
     * @var IGetOneSizeGateway
     */
    private $getOneSizeGateway;

    public function __construct(
        ICreateCreatureGateway $createEntityGateway,
        IGetOneAttackGateway $getOneAttackGateway,
        IGetOneColorGateway $getOneColorGateway,
        IGetOneCreatureGateway $getOneCreatureGateway,
        IGetOneEvolutionGateway $getOneEvolutionGateway,
        IGetOneLevelGateway $getOneLevelGateway,
        IGetOneRarityGateway $getOneRarityGateway,
        IGetOneSizeGateway $getOneSizeGateway
    ) {
        $this->createEntityGateway = $createEntityGateway;
        $this->getOneAttackGateway = $getOneAttackGateway;
        $this->getOneColorGateway = $getOneColorGateway;
        $this->getOneEvolutionGateway = $getOneEvolutionGateway;
        $this->getOneRarityGateway = $getOneRarityGateway;
        $this->getOneSizeGateway = $getOneSizeGateway;
        $this->getOneLevelGateway = $getOneLevelGateway;
        $this->getOneCreatureGateway = $getOneCreatureGateway;
    }

    public function execute(ICreateCreatureUseCaseInput $input, ICreateCreatureUseCaseOutput $output): void {
        $entity = new Creature();

        $entity->setAttack($this->getOneAttackGateway->getById($input->getAttackId()));
        $entity->setAttackRating($input->getAttackRating());
        $entity->setColor($this->getOneColorGateway->getById($input->getColorId()));
        $entity->setDefenseRating($input->getDefenseRating());
        $entity->setEvolution($this->getOneEvolutionGateway->getById($input->getEvolutionId()));
        $entity->setHealthPoints($input->getHealthPoints());
        $entity->setLevel($this->getOneLevelGateway->getById($input->getLevelId()));
        $entity->setLuckRating($input->getLuckRating());
        $entity->setManaPool($input->getManaPool());
        $entity->setName($input->getName());

        if ($input->getNextCreatureId() != null) {
            $entity->setNextCreature($this->getOneCreatureGateway->getById($input->getNextCreatureId()));
        }

        if ($input->getPreviousCreatureId() != null) {
            $entity->setPreviousCreature($this->getOneCreatureGateway->getById($input->getPreviousCreatureId()));
        }

        $entity->setPowerRating($input->getPowerRating());
        $entity->setRarity($this->getOneRarityGateway->getById($input->getRarityId()));
        $entity->setSize($this->getOneSizeGateway->getById($input->getSizeId()));

        $attacks = $this->createEntityGateway->execute($entity);

        $output->setCreature($attacks);
    }

}