<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/10/2018
 * Time: 4:11 PM
 */

namespace App\UseCase;

interface ICreateCreatureUseCaseInput {

    public function getAttackId(): ?string;

    public function getAttackRating(): ?int;

    public function getColorId(): ?string;

    public function getDefenseRating(): ?int;

    public function getEvolutionId(): ?string;

    public function getHealthPoints(): ?int;

    public function getLevelId(): ?string;

    public function getLuckRating(): ?int;

    public function getManaPool(): ?int;

    public function getName(): ?string;

    public function getNextCreatureId(): ?string;

    public function getPowerRating(): ?int;
    public function getPreviousCreatureId(): ?string;

    public function getRarityId(): ?string;

    public function getSizeId(): ?string;

    public function setAttackId(?string $attack): void;

    public function setAttackRating(?int $attackRating): void;

    public function setColorId(?string $color): void;

    public function setDefenseRating(?int $defenseRating): void;

    public function setEvolutionId(?string $evolution): void;

    public function setHealthPoints(?int $healthPoints): void;

    public function setLevelId(?string $level): void;

    public function setLuckRating(?int $luckRating): void;

    public function setManaPool(?int $manaPool): void;

    public function setName(?string $name): void;

    public function setNextCreatureId(?string $nextCreatureId): void;

    public function setPreviousCreatureId(?string $previousCreatureId): void;

    public function setPowerRating(?int $powerRating): void;

    public function setRarityId(?string $rarity): void;

    public function setSizeId(?string $size): void;
}