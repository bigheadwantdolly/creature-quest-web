<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 12:54 AM
 */

namespace App\UseCase;

use App\EntityGateway\IGetPreviousEvolutionGateway;
use App\EntityGateway\IGetPreviousEvolutionOptionsGateway;

class GetPreviousEvolutionOptionsUseCase implements IGetPreviousEvolutionOptionsUseCase {
    /**
     * @var IGetPreviousEvolutionGateway
     */
    private $getPreviousEvolutionGateway;

    /**
     * @var IGetPreviousEvolutionOptionsGateway
     */
    private $getPreviousEvolutionOptionsGateway;

    public function __construct(
        IGetPreviousEvolutionGateway $getPreviousEvolutionGateway,
        IGetPreviousEvolutionOptionsGateway $getPreviousEvolutionOptionsGateway
    ) {
        $this->getPreviousEvolutionOptionsGateway = $getPreviousEvolutionOptionsGateway;
        $this->getPreviousEvolutionGateway = $getPreviousEvolutionGateway;
    }

    public function execute(
        IGetPreviousEvolutionOptionsUseCaseInput $input,
        IGetPreviousEvolutionOptionsUseCaseOutput $output
    ): void {
        $previousEvolution = $this->getPreviousEvolutionGateway->execute($input->getEvolutionId());
        $creatures = [];

        if ($previousEvolution !== null) {
            $creatures = $this->getPreviousEvolutionOptionsGateway->execute(
                $input->getColorId(),
                $previousEvolution->getId(),
                $input->getSizeId()
            );
        }

        $output->setCreatures($creatures);
    }
}