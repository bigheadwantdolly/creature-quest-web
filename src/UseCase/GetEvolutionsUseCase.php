<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

use App\EntityGateway\IGetManyEvolutionGateway;

class GetEvolutionsUseCase implements IGetEvolutionsUseCase {

    /**
     * @var IGetManyEvolutionGateway
     */
    private $getAllEvolutionsEntityGateway;

    public function __construct(IGetManyEvolutionGateway $getAllEvolutionsEntityGateway) {
        $this->getAllEvolutionsEntityGateway = $getAllEvolutionsEntityGateway;
    }

    public function execute(IGetEvolutionsUseCaseInput $input, IGetEvolutionsUseCaseOutput $output): void {
        $evolutions = $this->getAllEvolutionsEntityGateway->execute($input->getRarityId(), $input->getSizeId());

        $output->setEvolutions($evolutions);
    }
}