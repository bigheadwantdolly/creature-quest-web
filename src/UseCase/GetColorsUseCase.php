<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

use App\EntityGateway\IGetManyColorGateway;

class GetColorsUseCase implements IGetColorsUseCase {

    /**
     * @var IGetManyColorGateway
     */
    private $getAllColorsEntityGateway;

    public function __construct(IGetManyColorGateway $getAllColorsEntityGateway) {
        $this->getAllColorsEntityGateway = $getAllColorsEntityGateway;
    }

    public function execute(IGetColorsUseCaseInput $input, IGetColorsUseCaseOutput $output): void {
        $colors = $this->getAllColorsEntityGateway->execute();

        $output->setColors($colors);
    }
}