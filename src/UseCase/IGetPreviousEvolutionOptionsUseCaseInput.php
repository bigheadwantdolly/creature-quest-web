<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 7/7/2018
 * Time: 2:52 PM
 */

namespace App\UseCase;

interface IGetPreviousEvolutionOptionsUseCaseInput {

    /**
     * @return string
     */
    public function getColorId(): string;

    /**
     * @return string
     */
    public function getEvolutionId(): string;

    /**
     * @return string
     */
    public function getSizeId(): string;

    /**
     * @param string $colorId
     */
    public function setColorId(string $colorId): void;

    /**
     * @param string $evolutionId
     */
    public function setEvolutionId(string $evolutionId): void;

    /**
     * @param string $sizeId
     */
    public function setSizeId(string $sizeId): void;

}