<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 1:10 AM
 */

namespace App\UseCase;

class GetPreviousEvolutionOptionsUseCaseInput implements IGetPreviousEvolutionOptionsUseCaseInput {

    /**
     * @var string
     */
    private $colorId;
    /**
     * @var string
     */
    private $evolutionId;
    /**
     * @var string
     */
    private $sizeId;

    /**
     * @return string
     */
    public function getColorId(): string {
        return $this->colorId;
    }

    /**
     * @return string
     */
    public function getEvolutionId(): string {
        return $this->evolutionId;
    }

    /**
     * @return string
     */
    public function getSizeId(): string {
        return $this->sizeId;
    }

    /**
     * @param string $colorId
     */
    public function setColorId(string $colorId): void {
        $this->colorId = $colorId;
    }

    /**
     * @param string $evolutionId
     */
    public function setEvolutionId(string $evolutionId): void {
        $this->evolutionId = $evolutionId;
    }

    /**
     * @param string $sizeId
     */
    public function setSizeId(string $sizeId): void {
        $this->sizeId = $sizeId;
    }
}