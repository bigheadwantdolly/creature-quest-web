<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:16 PM
 */

namespace App\UseCase;

use App\Entity\Attack;

interface IGetAttacksUseCaseOutput {

    /**
     * @return Attack[]
     */
    function getAttacks(): array;

    /**
     * @param Attack[] $attacks
     */
    function setAttacks(array $attacks): void;

}