<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:16 PM
 */

namespace App\UseCase;

use App\Entity\Rarity;

interface IGetRaritiesUseCaseOutput {

    /**
     * @return Rarity[]
     */
    function getRarities(): array;

    /**
     * @param Rarity[] $rarities
     */
    function setRarities(array $rarities): void;

}