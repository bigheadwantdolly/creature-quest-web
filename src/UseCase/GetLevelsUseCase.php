<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

use App\EntityGateway\IGetManyLevelGateway;

class GetLevelsUseCase implements IGetLevelsUseCase {

    /**
     * @var IGetManyLevelGateway
     */
    private $getAllLevelsEntityGateway;

    public function __construct(IGetManyLevelGateway $getAllLevelsEntityGateway) {
        $this->getAllLevelsEntityGateway = $getAllLevelsEntityGateway;
    }

    public function execute(IGetLevelsUseCaseInput $input, IGetLevelsUseCaseOutput $output): void {
        $levels = $this->getAllLevelsEntityGateway->execute($input->getEvolutionId(), $input->getSizeId());

        $output->setLevels($levels);
    }
}