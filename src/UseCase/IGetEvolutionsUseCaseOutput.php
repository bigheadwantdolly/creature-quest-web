<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:16 PM
 */

namespace App\UseCase;

use App\Entity\Evolution;

interface IGetEvolutionsUseCaseOutput {

    /**
     * @return Evolution[]
     */
    function getEvolutions(): array;

    /**
     * @param Evolution[] $evolutions
     */
    function setEvolutions(array $evolutions): void;

}