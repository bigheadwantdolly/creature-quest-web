<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

use App\Entity\Evolution;

class GetEvolutionsUseCaseOutput implements IGetEvolutionsUseCaseOutput {

    /**
     * @var Evolution[]
     */
    private $evolutions;

    public function __construct() {
        $this->evolutions = [];
    }

    /**
     * @return Evolution[]
     */
    function getEvolutions(): array {
        return $this->evolutions;
    }

    /**
     * @param Evolution[] $evolutions
     */
    function setEvolutions(array $evolutions): void {
        $this->evolutions = $evolutions;
    }
}