<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 12:57 AM
 */

namespace App\UseCase;

use App\Entity\Creature;

interface IGetCreaturesUseCaseOutput {

    /**
     * @return Creature[]
     */
    function getCreatures(): array;

    /**
     * @param Creature[] $creatures
     */
    function setCreatures(array $creatures): void;

}