<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

use App\EntityGateway\IGetManySizeGateway;

class GetSizesUseCase implements IGetSizesUseCase {

    /**
     * @var IGetManySizeGateway
     */
    private $getAllSizesEntityGateway;

    public function __construct(IGetManySizeGateway $getAllSizesEntityGateway) {
        $this->getAllSizesEntityGateway = $getAllSizesEntityGateway;
    }

    public function execute(IGetSizesUseCaseInput $input, IGetSizesUseCaseOutput $output): void {
        $sizes = $this->getAllSizesEntityGateway->execute();

        $output->setSizes($sizes);
    }
}