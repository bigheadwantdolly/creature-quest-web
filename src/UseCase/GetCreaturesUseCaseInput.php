<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/30/2018
 * Time: 1:10 AM
 */

namespace App\UseCase;

class GetCreaturesUseCaseInput implements IGetCreaturesUseCaseInput {

    /**
     * @var string
     */
    private $colorId = '';

    function getColorId(): string {
        return $this->colorId;
    }

    function setColorId(string $colorId): void {
        $this->colorId = $colorId;
    }
}