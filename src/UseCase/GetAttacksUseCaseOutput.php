<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:15 PM
 */

namespace App\UseCase;

use App\Entity\Attack;

class GetAttacksUseCaseOutput implements IGetAttacksUseCaseOutput {

    /**
     * @var Attack[]
     */
    private $attacks;

    public function __construct() {
        $this->attacks = [];
    }

    /**
     * @return Attack[]
     */
    function getAttacks(): array {
        return $this->attacks;
    }

    /**
     * @param Attack[] $attacks
     */
    function setAttacks(array $attacks): void {
        $this->attacks = $attacks;
    }
}