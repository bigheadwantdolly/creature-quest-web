<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/10/2018
 * Time: 4:11 PM
 */

namespace App\UseCase;

use App\Entity\Creature;

interface ICreateCreatureUseCaseOutput {

    function getCreature(): Creature;

    function setCreature(Creature $entity): void;
}