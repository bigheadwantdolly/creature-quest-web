<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/20/2018
 * Time: 5:31 PM
 */

namespace App\Util;

class ArrayHelper {

    static public function getValue(array $array, $index, $default = null) {
        $result = $default;

        if (isset($array[$index])) {
            $result = $array[$index];
        }

        return $result;
    }
}