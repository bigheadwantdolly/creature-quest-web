<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:23 PM
 */

namespace App\EntityGateway;

use App\Entity\Creature;

class GetOneCreatureGateway implements IGetOneCreatureGateway {

    /**
     * @var IGetManyCreatureGateway
     */
    private $entityGateway;

    public function __construct(IGetManyCreatureGateway $entityGateway) {
        $this->entityGateway = $entityGateway;
    }

    /**
     * @param string $id
     *
     * @return Creature
     */
    public function getById(string $id): ?Creature {
        $entities = $this->entityGateway->execute();
        $result = null;

        foreach ($entities as $entity) {
            if ($entity->getId() == $id) {
                $result = $entity;
                break;
            }
        }

        return $result;
    }
}