<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:17 PM
 */

namespace App\EntityGateway;

use App\Entity\Creature;

interface IGetOneCreatureGateway {

    /**
     * @param string $id
     *
     * @return Creature
     */
    public function getById(string $id): ?Creature;

}