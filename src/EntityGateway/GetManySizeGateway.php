<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:25 PM
 */

namespace App\EntityGateway;

use App\ApiRequest\IApiReadRequest;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\IEntityFactory;
use App\Entity\Size;
use App\EntityMapper\SizeMapper;

class GetManySizeGateway implements IGetManySizeGateway {

    /**
     * @var IApiReadRequest
     */
    private $apiRequest;
    /**
     * @var IApiRequestConfigurationFactory
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var IEntityFactory
     */
    private $entityFactory;
    /**
     * @var SizeMapper
     */
    private $entityMapper;

    public function __construct(
        IApiReadRequest $apiRequest,
        IApiRequestConfigurationFactory $apiRequestConfigurationFactory,
        IEntityFactory $entityFactory,
        SizeMapper $entityMapper
    ) {
        $this->apiRequest = $apiRequest;
        $this->apiRequestConfigurationFactory = $apiRequestConfigurationFactory;
        $this->entityFactory = $entityFactory;
        $this->entityMapper = $entityMapper;
    }

    /**
     * @return Size[]
     */
    public function execute(): array {
        $endpointUrlSuffix = 'sizes';
        $apiConfiguration = $this->apiRequestConfigurationFactory->createReadConfiguration($endpointUrlSuffix);
        $entityData = $this->apiRequest->execute($apiConfiguration);
        /**
         * @todo this needs to be passed off to a class that can return an iteratable list
         */
        $entityData = $entityData[$endpointUrlSuffix];
        $entities = [];

        $this->entityMapper->MapMany($entityData, $entities);

        return $entities;
    }
}