<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:17 PM
 */

namespace App\EntityGateway;

use App\Entity\Level;

interface IGetOneLevelGateway {

    /**
     * @param string $id
     *
     * @return Level
     */
    public function getById(string $id): ?Level;

}