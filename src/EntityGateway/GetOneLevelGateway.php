<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:23 PM
 */

namespace App\EntityGateway;

use App\Entity\Level;

class GetOneLevelGateway implements IGetOneLevelGateway {

    /**
     * @var IGetManyLevelGateway
     */
    private $entityGateway;

    public function __construct(IGetManyLevelGateway $entityGateway) {
        $this->entityGateway = $entityGateway;
    }

    /**
     * @param string $id
     *
     * @return Level
     */
    public function getById(string $id): ?Level {
        $entities = $this->entityGateway->execute();
        $result = null;

        foreach ($entities as $entity) {
            if ($entity->getId() == $id) {
                $result = $entity;
                break;
            }
        }

        return $result;
    }
}