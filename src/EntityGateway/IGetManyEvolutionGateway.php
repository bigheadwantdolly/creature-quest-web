<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:24 PM
 */

namespace App\EntityGateway;

use App\Entity\Evolution;

interface IGetManyEvolutionGateway {

    /**
     * @param string $rarityId
     * @param string $sizeId
     *
     * @return Evolution[]
     */
    public function execute(?string $rarityId = null, ?string $sizeId = null): array;
}