<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:23 PM
 */

namespace App\EntityGateway;

use App\Entity\Evolution;

class GetOneEvolutionGateway implements IGetOneEvolutionGateway {

    /**
     * @var IGetManyEvolutionGateway
     */
    private $entityGateway;

    public function __construct(IGetManyEvolutionGateway $entityGateway) {
        $this->entityGateway = $entityGateway;
    }

    /**
     * @param string $id
     *
     * @return Evolution
     */
    public function getById(string $id): ?Evolution {
        $entities = $this->entityGateway->execute();
        $result = null;

        foreach ($entities as $entity) {
            if ($entity->getId() == $id) {
                $result = $entity;
                break;
            }
        }

        return $result;
    }
}