<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 7/7/2018
 * Time: 2:54 PM
 */

namespace App\EntityGateway;

use App\Entity\Creature;

interface IGetNextEvolutionOptionsGateway {
    /**
     * @param null|string $colorId
     * @param null|string $evolutionId
     * @param null|string $sizeId
     *
     * @return Creature[]
     */
    public function execute(?string $colorId, ?string $evolutionId, ?string $sizeId): array;
}