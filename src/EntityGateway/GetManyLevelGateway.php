<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:25 PM
 */

namespace App\EntityGateway;

use App\ApiRequest\IApiReadRequest;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\IEntityFactory;
use App\Entity\Level;
use App\EntityMapper\LevelMapper;

class GetManyLevelGateway implements IGetManyLevelGateway {

    /**
     * @var IApiReadRequest
     */
    private $apiRequest;
    /**
     * @var IApiRequestConfigurationFactory
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var IEntityFactory
     */
    private $entityFactory;
    /**
     * @var LevelMapper
     */
    private $entityMapper;

    public function __construct(
        IApiReadRequest $apiRequest,
        IApiRequestConfigurationFactory $apiRequestConfigurationFactory,
        IEntityFactory $entityFactory,
        LevelMapper $entityMapper
    ) {
        $this->apiRequest = $apiRequest;
        $this->apiRequestConfigurationFactory = $apiRequestConfigurationFactory;
        $this->entityFactory = $entityFactory;
        $this->entityMapper = $entityMapper;
    }

    /**
     * @param null|string $evolutionId
     * @param null|string $sizeId
     *
     * @return Level[]
     */
    public function execute(?string $evolutionId = null, ?string $sizeId = null): array {
        $endpointUrlSuffix = 'levels';
        $parameters = [];

        if (!empty($evolutionId)) {
            $parameters['evolution'] = $evolutionId;
        }
        if (!empty($sizeId)) {
            $parameters['size'] = $sizeId;
        }

        $apiConfiguration = $this->apiRequestConfigurationFactory->createReadConfiguration(
            $endpointUrlSuffix,
            $parameters
        );
        $entityData = $this->apiRequest->execute($apiConfiguration);
        $entityData = $entityData[$endpointUrlSuffix];
        $entities = [];

        $this->entityMapper->MapMany($entityData, $entities);

        return $entities;
    }
}