<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:24 PM
 */

namespace App\EntityGateway;

use App\Entity\Rarity;

interface IGetManyRarityGateway {

    /**
     * @return Rarity[]
     */
    public function execute(): array;
}