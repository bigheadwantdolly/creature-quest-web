<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 11:52 AM
 */

namespace App\EntityGateway;

use App\Entity\Creature;

interface ICreateCreatureGateway {

    /**
     * @param Creature $entity
     *
     * @return Creature
     */
    public function execute(Creature $entity): Creature;

}