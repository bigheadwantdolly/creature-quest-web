<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:25 PM
 */

namespace App\EntityGateway;

use App\ApiRequest\IApiReadRequest;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\Evolution;
use App\Entity\IEntityFactory;
use App\EntityMapper\EvolutionMapper;

class GetManyEvolutionGateway implements IGetManyEvolutionGateway {

    /**
     * @var IApiReadRequest
     */
    private $apiRequest;
    /**
     * @var IApiRequestConfigurationFactory
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var IEntityFactory
     */
    private $entityFactory;
    /**
     * @var EvolutionMapper
     */
    private $entityMapper;

    public function __construct(
        IApiReadRequest $apiRequest,
        IApiRequestConfigurationFactory $apiRequestConfigurationFactory,
        IEntityFactory $entityFactory,
        EvolutionMapper $entityMapper
    ) {
        $this->apiRequest = $apiRequest;
        $this->apiRequestConfigurationFactory = $apiRequestConfigurationFactory;
        $this->entityFactory = $entityFactory;
        $this->entityMapper = $entityMapper;
    }

    /**
     * @param null|string $rarityId
     * @param null|string $sizeId
     *
     * @return Evolution[]
     */
    public function execute(?string $rarityId = null, ?string $sizeId = null): array {
        $endpointUrlSuffix = 'evolutions';
        $parameters = [];

        if (!empty($rarityId)) {
            $parameters['rarity'] = $rarityId;
        }
        if (!empty($sizeId)) {
            $parameters['size'] = $sizeId;
        }

        $apiConfiguration = $this->apiRequestConfigurationFactory->createReadConfiguration(
            $endpointUrlSuffix,
            $parameters
        );
        $entityData = $this->apiRequest->execute($apiConfiguration);
        /**
         * @todo this needs to be passed off to a class that can return an iteratable list
         */
        $entityData = $entityData[$endpointUrlSuffix];
        $entities = [];

        $this->entityMapper->MapMany($entityData, $entities);

        return $entities;
    }
}