<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:23 PM
 */

namespace App\EntityGateway;

use App\Entity\Color;

class GetOneColorGateway implements IGetOneColorGateway {

    /**
     * @var IGetManyColorGateway
     */
    private $entityGateway;

    public function __construct(IGetManyColorGateway $entityGateway) {
        $this->entityGateway = $entityGateway;
    }

    /**
     * @param string $id
     *
     * @return Color
     */
    public function getById(string $id): ?Color {
        $entities = $this->entityGateway->execute();
        $result = null;

        foreach ($entities as $entity) {
            if ($entity->getId() == $id) {
                $result = $entity;
                break;
            }
        }

        return $result;
    }
}