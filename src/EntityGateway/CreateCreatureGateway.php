<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/9/2018
 * Time: 11:53 AM
 */

namespace App\EntityGateway;

use App\ApiRequest\IApiCreateRequest;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\Creature;
use App\Entity\IEntityFactory;
use App\EntityMapper\CreatureMapper;
use InvalidArgumentException;

class CreateCreatureGateway implements ICreateCreatureGateway {

    public const PARAMETER_ATTACK_ID = 'AttackId';
    public const PARAMETER_ATTACK_RATING = 'AttackRating';
    public const PARAMETER_COLOR_ID = 'ColorId';
    public const PARAMETER_DEFENSE_RATING = 'DefenseRating';
    public const PARAMETER_EVOLUTION_ID = 'EvolutionId';
    public const PARAMETER_HEALTH_POINTS = 'HealthPoints';
    public const PARAMETER_LEVEL_ID = 'LevelId';
    public const PARAMETER_LUCK_RATING = 'LuckRating';
    public const PARAMETER_MANA_POOL = 'ManaPool';
    public const PARAMETER_NAME = 'Name';
    public const PARAMETER_NEXT_CREATURE_ID = 'NextCreatureId';
    public const PARAMETER_POWER_RATING = 'PowerRating';
    public const PARAMETER_PREVIOUS_CREATURE_ID = 'PreviousCreatureId';
    public const PARAMETER_RARITY_ID = 'RarityId';
    public const PARAMETER_SIZE_ID = 'SizeId';

    /**
     * @var IApiCreateRequest
     */
    private $apiCreateRequest;
    /**
     * @var IApiRequestConfigurationFactory
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var IEntityFactory
     */
    private $entityFactory;
    /**
     * @var CreatureMapper
     */
    private $entityMapper;

    public function __construct(
        IApiCreateRequest $apiCreateRequest,
        IApiRequestConfigurationFactory $apiRequestConfigurationFactory,
        IEntityFactory $entityFactory,
        CreatureMapper $entityMapper
    ) {
        $this->apiCreateRequest = $apiCreateRequest;
        $this->apiRequestConfigurationFactory = $apiRequestConfigurationFactory;
        $this->entityFactory = $entityFactory;
        $this->entityMapper = $entityMapper;
    }

    public function execute(Creature $entity): Creature {
        $endpointUrlSuffix = 'creatures';
        $parameters = [
            self::PARAMETER_ATTACK_RATING => $entity->getAttackRating(),
            self::PARAMETER_DEFENSE_RATING => $entity->getDefenseRating(),
            self::PARAMETER_HEALTH_POINTS => $entity->getHealthPoints(),
            self::PARAMETER_LUCK_RATING => $entity->getLuckRating(),
            self::PARAMETER_MANA_POOL => $entity->getManaPool(),
            self::PARAMETER_NAME => $entity->getName(),
            self::PARAMETER_POWER_RATING => $entity->getPowerRating()
        ];

        if ($entity->getAttack() == null) {
            throw new InvalidArgumentException(
                sprintf('the `%s` entity requires an `attack` value', get_class($entity))
            );
        }

        if ($entity->getColor() == null) {
            throw new InvalidArgumentException(
                sprintf('the `%s` entity requires a `color` value', get_class($entity))
            );
        }

        if ($entity->getEvolution() == null) {
            throw new InvalidArgumentException(
                sprintf('the `%s` entity requires an `evolution` value', get_class($entity))
            );
        }

        if ($entity->getLevel() == null) {
            throw new InvalidArgumentException(
                sprintf('the `%s` entity requires a `level` value', get_class($entity))
            );
        }

        if (empty($entity->getName())) {
            throw new InvalidArgumentException(
                sprintf('the `%s` entity requires a `name` value', get_class($entity))
            );
        }

        if ($entity->getRarity() == null) {
            throw new InvalidArgumentException(
                sprintf('the `%s` entity requires a `rarity` value', get_class($entity))
            );
        }

        if ($entity->getSize() == null) {
            throw new InvalidArgumentException(sprintf('the `%s` entity requires an `size` value', get_class($entity)));
        }

        $parameters[self::PARAMETER_ATTACK_ID] = $entity->getAttack()->getId();
        $parameters[self::PARAMETER_COLOR_ID] = $entity->getColor()->getId();
        $parameters[self::PARAMETER_EVOLUTION_ID] = $entity->getEvolution()->getId();
        $parameters[self::PARAMETER_LEVEL_ID] = $entity->getLevel()->getId();

        if ($entity->getNextCreature() != null) {
            $parameters[self::PARAMETER_NEXT_CREATURE_ID] = $entity->getNextCreature()->getId();
        }

        if ($entity->getPreviousCreature() != null) {
            $parameters[self::PARAMETER_PREVIOUS_CREATURE_ID] = $entity->getPreviousCreature()->getId();
        }

        $parameters[self::PARAMETER_RARITY_ID] = $entity->getRarity()->getId();
        $parameters[self::PARAMETER_SIZE_ID] = $entity->getSize()->getId();

        $apiConfiguration = $this->apiRequestConfigurationFactory->createCreateConfiguration(
            $endpointUrlSuffix,
            $parameters
        );
        $entityData = $this->apiCreateRequest->execute($apiConfiguration);
        $entityData = $entityData['creature'];
        $entity = $this->entityFactory->create(Creature::class);

        $this->entityMapper->mapOne($entityData, $entity);

        return $entity;
    }
}