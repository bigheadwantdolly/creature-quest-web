<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:24 PM
 */

namespace App\EntityGateway;

use App\Entity\Size;

interface IGetManySizeGateway {

    /**
     * @return Size[]
     */
    public function execute(): array;
}