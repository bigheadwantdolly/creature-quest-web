<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:16 PM
 */

namespace App\EntityGateway;

use App\Entity\Attack;

interface IGetOneAttackGateway {

    /**
     * @param string $id
     *
     * @return Attack
     */
    public function getById(string $id): ?Attack;

}