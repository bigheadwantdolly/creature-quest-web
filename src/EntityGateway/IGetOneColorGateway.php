<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:17 PM
 */

namespace App\EntityGateway;

use App\Entity\Color;

interface IGetOneColorGateway {

    /**
     * @param string $id
     *
     * @return Color
     */
    public function getById(string $id): ?Color;

}