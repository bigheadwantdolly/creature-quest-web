<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 7/8/2018
 * Time: 1:24 AM
 */

namespace App\EntityGateway;

use App\Entity\Evolution;

interface IGetPreviousEvolutionGateway {

    /**
     * @param string $id
     *
     * @return Evolution
     */
    public function execute(string $id): ?Evolution;
}