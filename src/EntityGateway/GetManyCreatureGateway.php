<?php
/**
 * Created by PhpStorm.
 * User: adamgross
 * Date: 5/30/18
 * Time: 8:43 PM
 */

namespace App\EntityGateway;

use App\ApiRequest\IApiReadRequest;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\Creature;
use App\Entity\IEntityFactory;
use App\EntityMapper\CreatureMapper;

class GetManyCreatureGateway implements IGetManyCreatureGateway {

    /**
     * @var IApiReadRequest
     */
    private $apiRequest;
    /**
     * @var IApiRequestConfigurationFactory
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var IEntityFactory
     */
    private $entityFactory;
    /**
     * @var CreatureMapper
     */
    private $entityMapper;

    public function __construct(
        IApiReadRequest $apiRequest,
        IApiRequestConfigurationFactory $apiRequestConfigurationFactory,
        IEntityFactory $entityFactory,
        CreatureMapper $entityMapper
    ) {
        $this->apiRequest = $apiRequest;
        $this->apiRequestConfigurationFactory = $apiRequestConfigurationFactory;
        $this->entityFactory = $entityFactory;
        $this->entityMapper = $entityMapper;
    }

    /**
     * @param string[] $colorFilters
     *
     * @return Creature[]
     */
    public function execute(array $colorFilters = []): array {
        $endpointUrlSuffix = 'creatures';
        $parameters = [
            'color' => $colorFilters
        ];

        $apiConfiguration = $this->apiRequestConfigurationFactory->createReadConfiguration(
            $endpointUrlSuffix,
            $parameters
        );
        $entityData = $this->apiRequest->execute($apiConfiguration);
        /**
         * @todo this needs to be passed off to a class that can return an iteratable list
         */
        $entityData = $entityData[$endpointUrlSuffix];
        $entities = [];

        $this->entityMapper->MapMany($entityData, $entities);

        return $entities;
    }
}