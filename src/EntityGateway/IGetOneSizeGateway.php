<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:17 PM
 */

namespace App\EntityGateway;

use App\Entity\Size;

interface IGetOneSizeGateway {

    /**
     * @param string $id
     *
     * @return Size
     */
    public function getById(string $id): ?Size;

}