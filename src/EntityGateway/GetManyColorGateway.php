<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/2/2018
 * Time: 2:25 PM
 */

namespace App\EntityGateway;

use App\ApiRequest\IApiReadRequest;
use App\ApiRequest\IApiRequestConfigurationFactory;
use App\Entity\Color;
use App\Entity\IEntityFactory;
use App\EntityMapper\ColorMapper;

class GetManyColorGateway implements IGetManyColorGateway {

    /**
     * @var IApiReadRequest
     */
    private $apiRequest;
    /**
     * @var IApiRequestConfigurationFactory
     */
    private $apiRequestConfigurationFactory;
    /**
     * @var IEntityFactory
     */
    private $entityFactory;
    /**
     * @var ColorMapper
     */
    private $entityMapper;

    public function __construct(
        IApiReadRequest $apiRequest,
        IApiRequestConfigurationFactory $apiRequestConfigurationFactory,
        IEntityFactory $entityFactory,
        ColorMapper $entityMapper
    ) {
        $this->apiRequest = $apiRequest;
        $this->apiRequestConfigurationFactory = $apiRequestConfigurationFactory;
        $this->entityFactory = $entityFactory;
        $this->entityMapper = $entityMapper;
    }

    /**
     * @return Color[]
     */
    public function execute(): array {
        $endpointUrlSuffix = 'colors';
        $apiConfiguration = $this->apiRequestConfigurationFactory->createReadConfiguration($endpointUrlSuffix);
        $entityData = $this->apiRequest->execute($apiConfiguration);
        /**
         * @todo this needs to be passed off to a class that can return an iteratable list
         */
        $entityData = $entityData[$endpointUrlSuffix];
        $entities = [];

        $this->entityMapper->MapMany($entityData, $entities);

        return $entities;
    }
}