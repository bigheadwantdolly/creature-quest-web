<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/12/2018
 * Time: 5:17 PM
 */

namespace App\EntityGateway;

use App\Entity\Evolution;

interface IGetOneEvolutionGateway {

    /**
     * @param string $id
     *
     * @return Evolution
     */
    public function getById(string $id): ?Evolution;

}