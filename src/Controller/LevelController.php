<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/21/2018
 * Time: 1:18 AM
 */

namespace App\Controller;

use App\UseCase\GetLevelsUseCaseInput;
use App\UseCase\GetLevelsUseCaseOutput;
use App\UseCase\IGetLevelsUseCase;
use Symfony\Component\HttpFoundation\Request;

class LevelController extends CQController {

    /**
     * @var IGetLevelsUseCase
     */
    private $getLevelsUseCase;

    public function __construct(IGetLevelsUseCase $getLevelsUseCase) {
        $this->getLevelsUseCase = $getLevelsUseCase;
    }

    public function handleGetRequest(Request $request) {
        $levelsUseCaseInput = new GetLevelsUseCaseInput();
        $levelsUseCaseOutput = new GetLevelsUseCaseOutput();
        $evolution = $request->get('evolution');
        $size = $request->get('size');

        $levelsUseCaseInput->setEvolutionId($evolution);
        $levelsUseCaseInput->setSizeId($size);

        $this->getLevelsUseCase->execute($levelsUseCaseInput, $levelsUseCaseOutput);

        return $this->json($levelsUseCaseOutput);
    }

}