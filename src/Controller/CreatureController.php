<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 6/3/2018
 * Time: 7:10 PM
 */

namespace App\Controller;

use App\UseCase\CreateCreatureUseCaseInput;
use App\UseCase\CreateCreatureUseCaseOutput;
use App\UseCase\GetAttacksUseCaseInput;
use App\UseCase\GetAttacksUseCaseOutput;
use App\UseCase\GetColorsUseCaseInput;
use App\UseCase\GetColorsUseCaseOutput;
use App\UseCase\GetCreaturesUseCaseInput;
use App\UseCase\GetCreaturesUseCaseOutput;
use App\UseCase\GetEvolutionsUseCaseInput;
use App\UseCase\GetEvolutionsUseCaseOutput;
use App\UseCase\GetLevelsUseCaseInput;
use App\UseCase\GetLevelsUseCaseOutput;
use App\UseCase\GetNextEvolutionOptionsUseCaseInput;
use App\UseCase\GetNextEvolutionOptionsUseCaseOutput;
use App\UseCase\GetPreviousEvolutionOptionsUseCaseInput;
use App\UseCase\GetPreviousEvolutionOptionsUseCaseOutput;
use App\UseCase\GetRaritiesUseCaseInput;
use App\UseCase\GetRaritiesUseCaseOutput;
use App\UseCase\GetSizesUseCaseInput;
use App\UseCase\GetSizesUseCaseOutput;
use App\UseCase\ICreateCreatureUseCase;
use App\UseCase\IGetAttacksUseCase;
use App\UseCase\IGetColorsUseCase;
use App\UseCase\IGetCreaturesUseCase;
use App\UseCase\IGetEvolutionsUseCase;
use App\UseCase\IGetLevelsUseCase;
use App\UseCase\IGetNextEvolutionOptionsUseCase;
use App\UseCase\IGetPreviousEvolutionOptionsUseCase;
use App\UseCase\IGetRaritiesUseCase;
use App\UseCase\IGetSizesUseCase;
use Symfony\Component\HttpFoundation\Request;

class CreatureController extends CQController {
    /**
     * @var ICreateCreatureUseCase
     */
    private $createCreatureUseCase;
    /**
     * @var IGetAttacksUseCase
     */
    private $getAttacksUseCase;
    /**
     * @var IGetColorsUseCase
     */
    private $getColorsUseCase;
    /**
     * @var IGetCreaturesUseCase
     */
    private $getCreaturesUseCase;
    /**
     * @var IGetEvolutionsUseCase
     */
    private $getEvolutionsUseCase;
    /**
     * @var IGetLevelsUseCase
     */
    private $getLevelsUseCase;
    /**
     * @var IGetNextEvolutionOptionsUseCase
     */
    private $getNextEvolutionOptionsUseCase;
    /**
     * @var IGetPreviousEvolutionOptionsUseCase
     */
    private $getPreviousEvolutionOptionsUseCase;
    /**
     * @var IGetRaritiesUseCase
     */
    private $getRaritiesUseCase;
    /**
     * @var IGetSizesUseCase
     */
    private $getSizesUseCase;

    public function __construct(
        ICreateCreatureUseCase $createCreatureUseCase,
        IGetAttacksUseCase $getAttacksUseCase,
        IGetColorsUseCase $getColorsUseCase,
        IGetCreaturesUseCase $getCreaturesUseCase,
        IGetEvolutionsUseCase $getEvolutionsUseCase,
        IGetLevelsUseCase $getLevelsUseCase,
        IGetNextEvolutionOptionsUseCase $getNextEvolutionOptionsUseCase,
        IGetPreviousEvolutionOptionsUseCase $getPreviousEvolutionOptionsUseCase,
        IGetRaritiesUseCase $getRaritiesUseCase,
        IGetSizesUseCase $getSizesUseCase
    ) {
        $this->createCreatureUseCase = $createCreatureUseCase;
        $this->getAttacksUseCase = $getAttacksUseCase;
        $this->getColorsUseCase = $getColorsUseCase;
        $this->getCreaturesUseCase = $getCreaturesUseCase;
        $this->getEvolutionsUseCase = $getEvolutionsUseCase;
        $this->getLevelsUseCase = $getLevelsUseCase;
        $this->getRaritiesUseCase = $getRaritiesUseCase;
        $this->getSizesUseCase = $getSizesUseCase;
        $this->getPreviousEvolutionOptionsUseCase = $getPreviousEvolutionOptionsUseCase;
        $this->getNextEvolutionOptionsUseCase = $getNextEvolutionOptionsUseCase;
    }

    public function handleCreateGetRequest(Request $request) {
        $attacksUseCaseInput = new GetAttacksUseCaseInput();
        $attacksUseCaseOutput = new GetAttacksUseCaseOutput();
        $colorsUseCaseInput = new GetColorsUseCaseInput();
        $colorsUseCaseOutput = new GetColorsUseCaseOutput();
        $evolutionsUseCaseInput = new GetEvolutionsUseCaseInput();
        $evolutionsUseCaseOutput = new GetEvolutionsUseCaseOutput();
        $levelsUseCaseInput = new GetLevelsUseCaseInput();
        $levelsUseCaseOutput = new GetLevelsUseCaseOutput();
        $raritiesUseCaseInput = new GetRaritiesUseCaseInput();
        $raritiesUseCaseOutput = new GetRaritiesUseCaseOutput();
        $sizesUseCaseInput = new GetSizesUseCaseInput();
        $sizesUseCaseOutput = new GetSizesUseCaseOutput();

        $callbackUrl = null;
        $evolutionsCallbackUrl = null;
        $levelsCallbackUrl = null;
        $nextEvolutionOptionsCallbackUrl = null;
        $previousEvolutionOptionsCallbackUrl = null;
        $postUrl = $this->generateUrl('app_creature_creation_submission_filter');

        $this->getAttacksUseCase->execute($attacksUseCaseInput, $attacksUseCaseOutput);
        $this->getColorsUseCase->execute($colorsUseCaseInput, $colorsUseCaseOutput);
        $this->getEvolutionsUseCase->execute($evolutionsUseCaseInput, $evolutionsUseCaseOutput);
        $this->getLevelsUseCase->execute($levelsUseCaseInput, $levelsUseCaseOutput);
        $this->getRaritiesUseCase->execute($raritiesUseCaseInput, $raritiesUseCaseOutput);
        $this->getSizesUseCase->execute($sizesUseCaseInput, $sizesUseCaseOutput);

        if (!$request->isXmlHttpRequest()) {
            $callbackUrl = $this->generateUrl('app_creature_creation_filter');
            $evolutionsCallbackUrl = $this->generateUrl('app_evolution_get');
            $levelsCallbackUrl = $this->generateUrl('app_level_get');
            $nextEvolutionOptionsCallbackUrl = $this->generateUrl('app_get_next_evolution_options');
            $previousEvolutionOptionsCallbackUrl = $this->generateUrl('app_get_previous_evolution_options');
        }

        return $this->render(
            'components/creatureCreation.html.twig',
            array(
                'callbackUrl' => $callbackUrl,
                'evolutionsCallbackUrl' => $evolutionsCallbackUrl,
                'levelsCallbackUrl' => $levelsCallbackUrl,
                'nextEvolutionOptionsCallbackUrl' => $nextEvolutionOptionsCallbackUrl,
                'previousEvolutionOptionsCallbackUrl' => $previousEvolutionOptionsCallbackUrl,
                'postUrl' => $postUrl,
                'attacks' => $attacksUseCaseOutput->getAttacks(),
                'colors' => $colorsUseCaseOutput->getColors(),
                'evolutions' => $evolutionsUseCaseOutput->getEvolutions(),
                'levels' => $levelsUseCaseOutput->getLevels(),
                'rarities' => $raritiesUseCaseOutput->getRarities(),
                'sizes' => $sizesUseCaseOutput->getSizes()
            )
        );
    }

    public function handleCreatePostRequest(Request $request) {
        $createCreatureUseCaseInput = new CreateCreatureUseCaseInput();
        $createCreatureUseCaseOutput = new CreateCreatureUseCaseOutput();

        $attackId = $request->get('attack');
        $attackRating = $request->get('attackRating');
        $colorId = $request->get('color');
        $defenseRating = $request->get('defenseRating');
        $evolutionId = $request->get('evolution');
        $healthPoints = $request->get('healthPoints');
        $levelId = $request->get('level');
        $luckRating = $request->get('luckRating');
        $manaPool = $request->get('manaPool');
        $name = $request->get('name');
        $powerRating = $request->get('powerRating');
        $rarityId = $request->get('rarity');
        $sizeId = $request->get('size');
        $nextCreatureId = $request->get('nextCreature');
        $previousCreatureId = $request->get('previousCreature');

        $createCreatureUseCaseInput->setAttackId($attackId);
        $createCreatureUseCaseInput->setAttackRating($attackRating);
        $createCreatureUseCaseInput->setColorId($colorId);
        $createCreatureUseCaseInput->setDefenseRating($defenseRating);
        $createCreatureUseCaseInput->setEvolutionId($evolutionId);
        $createCreatureUseCaseInput->setHealthPoints($healthPoints);
        $createCreatureUseCaseInput->setLevelId($levelId);
        $createCreatureUseCaseInput->setLuckRating($luckRating);
        $createCreatureUseCaseInput->setManaPool($manaPool);
        $createCreatureUseCaseInput->setName($name);
        $createCreatureUseCaseInput->setNextCreatureId($nextCreatureId);
        $createCreatureUseCaseInput->setPowerRating($powerRating);
        $createCreatureUseCaseInput->setPreviousCreatureId($previousCreatureId);
        $createCreatureUseCaseInput->setRarityId($rarityId);
        $createCreatureUseCaseInput->setSizeId($sizeId);

        $this->createCreatureUseCase->execute($createCreatureUseCaseInput, $createCreatureUseCaseOutput);

        return $this->json($createCreatureUseCaseOutput);
    }

    public function handleListingFilterGetRequest(Request $request) {
        $colorsUseCaseInput = new GetColorsUseCaseInput();
        $colorsUseCaseOutput = new GetColorsUseCaseOutput();

        $this->getColorsUseCase->execute($colorsUseCaseInput, $colorsUseCaseOutput);
        $callbackUrl = null;

        if (!$request->isXmlHttpRequest()) {
            $callbackUrl = $this->generateUrl('app_creature_listing_filter');
        }

        return $this->render(
            'components/creatureListingFilter.html.twig',
            array(
                'callbackUrl' => $callbackUrl,
                'colors' => $colorsUseCaseOutput->getColors()
            )
        );
    }

    public function handleListingGetRequest(Request $request) {
        $creaturesUseCaseInput = new GetCreaturesUseCaseInput();
        $creaturesUseCaseOutput = new GetCreaturesUseCaseOutput();
        $color = $request->get('color');

        if (!empty($color)) {
            $creaturesUseCaseInput->setColorId($color);
        }

        $this->getCreaturesUseCase->execute($creaturesUseCaseInput, $creaturesUseCaseOutput);
        $callbackUrl = null;

        if (!$request->isXmlHttpRequest()) {
            $callbackUrl = $this->generateUrl('app_creature_listing');
        }

        return $this->render(
            'components/creatureListing.html.twig',
            array(
                'callbackUrl' => $callbackUrl,
                'creatures' => $creaturesUseCaseOutput->getCreatures()
            )
        );
    }

    public function handleNextEvolutionOptionsGetRequest(Request $request) {
        $colorId = $request->get('color');
        $evolutionId = $request->get('evolution');
        $sizeId = $request->get('size');
        $useCaseInput = new GetNextEvolutionOptionsUseCaseInput();
        $useCaseOutput = new GetNextEvolutionOptionsUseCaseOutput();

        $useCaseInput->setColorId($colorId);
        $useCaseInput->setEvolutionId($evolutionId);
        $useCaseInput->setSizeId($sizeId);

        $this->getNextEvolutionOptionsUseCase->execute($useCaseInput, $useCaseOutput);

        return $this->json($useCaseOutput);
    }

    public function handlePreviousEvolutionOptionsGetRequest(Request $request) {
        $colorId = $request->get('color');
        $evolutionId = $request->get('evolution');
        $sizeId = $request->get('size');
        $useCaseInput = new GetPreviousEvolutionOptionsUseCaseInput();
        $useCaseOutput = new GetPreviousEvolutionOptionsUseCaseOutput();

        $useCaseInput->setColorId($colorId);
        $useCaseInput->setEvolutionId($evolutionId);
        $useCaseInput->setSizeId($sizeId);

        $this->getPreviousEvolutionOptionsUseCase->execute($useCaseInput, $useCaseOutput);

        return $this->json($useCaseOutput);
    }

}