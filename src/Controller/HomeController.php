<?php
/**
 * Created by PhpStorm.
 * User: Adam
 * Date: 5/29/2018
 * Time: 11:56 PM
 */

namespace App\Controller;

class HomeController extends CQController {

    public function index() {
        return $this->render('homePage.html.twig');
    }
}