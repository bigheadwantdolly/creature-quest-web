class CreateCreatureMediator extends Mediator {
    constructor(sender, receiver) {
        super(sender, receiver);
    }

    mediate() {
        this.sender.refresh();
        this.receiver.refresh();
    }
}