class CreatureListingFilterMediator extends Mediator {
    constructor(sender, receiver) {
        super(sender, receiver);
    }

    mediate() {
        this.receiver.color = this.sender.colorFilter;
        this.receiver.refresh();
    }
}