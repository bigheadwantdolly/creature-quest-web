class CreatureListingComponent extends Component {
    constructor(documentElementId, ajaxHelper) {
        super(documentElementId, ajaxHelper);

        this.color = null;
    }

    get color() {
        return this.getState('color');
    }

    set color(color) {
        this.setState('color', color);
    }

}