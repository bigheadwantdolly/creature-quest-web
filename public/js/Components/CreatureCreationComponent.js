class CreatureCreationComponent extends Component {
    constructor(documentElementId, ajaxHelper) {
        super(documentElementId, ajaxHelper);

        this.evolutionsCallbackUrl = null;
        this.levelsCallbackUrl = null;
        this.nextEvolutionOptionsCallbackUrl = null;
        this.previousEvolutionOptionsCallbackUrl = null;
    }

    init() {
        super.init();

        let form = this.getFormElement();
        let colorControl = form.find('select[name=color]');
        let sizeControl = form.find('select[name=size]');
        let rarityControl = form.find('select[name=rarity]');
        let evolutionsControl = form.find('select[name=evolution]');

        if (this.evolutionsCallbackUrl === null) {
            this.evolutionsCallbackUrl = evolutionsControl.attr('callback');
            evolutionsControl.removeAttr('callback');
        }

        if (this.levelsCallbackUrl === null) {
            let levelsControl = form.find('select[name=level]');

            this.levelsCallbackUrl = levelsControl.attr('callback');
            levelsControl.removeAttr('callback');
        }

        if (this.nextEvolutionOptionsCallbackUrl === null) {
            let nextEvolutionControl = form.find('select[name=nextEvolution]');

            this.nextEvolutionOptionsCallbackUrl = nextEvolutionControl.attr('callback');
            nextEvolutionControl.removeAttr('callback');
        }

        if (this.previousEvolutionOptionsCallbackUrl === null) {
            let previousEvolutionControl = form.find('select[name=previousEvolution]');

            this.previousEvolutionOptionsCallbackUrl = previousEvolutionControl.attr('callback');
            previousEvolutionControl.removeAttr('callback');
        }

        form.on('submit', (event) => {
            this.handleFormSubmission();
            event.preventDefault();
        });

        colorControl.on('change', (event) => {
            this.refreshNextEvolutions();
            this.refreshPreviousEvolutions();
            event.preventDefault();
        });

        sizeControl.on('change', (event) => {
            this.refreshEvolutions();
            this.refreshLevels();
            this.refreshNextEvolutions();
            this.refreshPreviousEvolutions();
            event.preventDefault();
        });

        evolutionsControl.on('change', (event) => {
            this.refreshLevels();
            this.refreshNextEvolutions();
            this.refreshPreviousEvolutions();
            event.preventDefault();
        });

        rarityControl.on('change', (event) => {
            this.refreshEvolutions();
            event.preventDefault();
        });
    }

    refreshNextEvolutions() {
        let form = this.getFormElement();
        let url = this.nextEvolutionOptionsCallbackUrl;
        let formData = {};

        formData.color = form.find('select[name=color]').val();
        formData.evolution = form.find('select[name=evolution]').val();
        formData.size = form.find('select[name=size]').val();

        if (formData.color !== null && formData.evolution !== null && formData.size !== null) {
            this.ajaxHelper.get(url, formData, AjaxHelper.JSON, (data, textStatus, jqXHR) => {
                let entities = data['creatures'];
                let form = this.getFormElement();
                let targetControl = form.find('select[name=nextEvolution]');
                let currentValue = targetControl.val();

                targetControl.empty().append('<option disabled="disabled" selected="selected" value="">Select</option>');

                for (let a = 0; a < entities.length; a++) {
                    let entity = entities[a];
                    let id = entity['id'];
                    let name = entity['name'];

                    targetControl.append('<option ' + (currentValue === id ? 'selected="selected"' : '') + ' value="' + id + '">' + name + '</option>');
                }
            });
        }
    }

    refreshPreviousEvolutions() {
        let form = this.getFormElement();
        let url = this.previousEvolutionOptionsCallbackUrl;
        let formData = {};

        formData.color = form.find('select[name=color]').val();
        formData.evolution = form.find('select[name=evolution]').val();
        formData.size = form.find('select[name=size]').val();

        if (formData.color !== null && formData.evolution !== null && formData.size !== null) {
            this.ajaxHelper.get(url, formData, AjaxHelper.JSON, (data, textStatus, jqXHR) => {
                let entities = data['creatures'];
                let form = this.getFormElement();
                let targetControl = form.find('select[name=previousEvolution]');
                let currentValue = targetControl.val();

                targetControl.empty().append('<option disabled="disabled" selected="selected" value="">Select</option>');

                for (let a = 0; a < entities.length; a++) {
                    let entity = entities[a];
                    let id = entity['id'];
                    let name = entity['name'];

                    targetControl.append('<option ' + (currentValue === id ? 'selected="selected"' : '') + ' value="' + id + '">' + name + '</option>');
                }
            });
        }
    }

    refreshEvolutions() {
        let form = this.getFormElement();
        let url = this.evolutionsCallbackUrl;
        let formData = {};

        formData.size = form.find('select[name=size]').val();
        formData.rarity = form.find('select[name=rarity]').val();

        if (formData.size !== null && formData.rarity !== null) {
            this.ajaxHelper.get(url, formData, AjaxHelper.JSON, (data, textStatus, jqXHR) => {
                let evolutions = data['evolutions'];
                let form = this.getFormElement();
                let targetControl = form.find('select[name=evolution]');
                let currentValue = targetControl.val();

                targetControl.empty().append('<option disabled="disabled" selected="selected" value="">Select</option>');

                for (let a = 0; a < evolutions.length; a++) {
                    let evolution = evolutions[a];
                    let id = evolution['id'];
                    let cardinality = evolution['cardinality'];

                    targetControl.append('<option ' + (currentValue === id ? 'selected="selected"' : '') + ' value="' + id + '">' + cardinality + '</option>');
                }
            });
        }
    }

    refreshLevels() {
        let form = this.getFormElement();
        let url = this.levelsCallbackUrl;
        let formData = {};

        formData.evolution = form.find('select[name=evolution]').val();
        formData.size = form.find('select[name=size]').val();

        if (formData.evolution !== null && formData.size !== null) {
            this.ajaxHelper.get(url, formData, AjaxHelper.JSON, (data, textStatus, jqXHR) => {
                let levels = data['levels'];
                let form = this.getFormElement();
                let targetControl = form.find('select[name=level]');
                let currentValue = targetControl.val();

                targetControl.empty().append('<option disabled="disabled" selected="selected" value="">Select</option>');

                for (let a = 0; a < levels.length; a++) {
                    let level = levels[a];
                    let id = level['id'];
                    let number = level['number'];

                    targetControl.append('<option ' + (currentValue === id ? 'selected="selected"' : '') + ' value="' + id + '">' + number + '</option>');
                }
            });
        }
    }

    getFormElement() {
        return this.documentElement.find('form');
    }

    handleFormSubmission() {
        let form = this.getFormElement();
        let url = form.attr('action');
        let formData = {};

        formData.name = form.find('input[name=name]').val();
        formData.color = form.find('select[name=color]').val();
        formData.size = form.find('select[name=size]').val();
        formData.rarity = form.find('select[name=rarity]').val();
        formData.attack = form.find('select[name=attack]').val();
        formData.evolution = form.find('select[name=evolution]').val();
        formData.level = form.find('select[name=level]').val();
        formData.attackRating = form.find('input[name=attackRating]').val();
        formData.defenseRating = form.find('input[name=defenseRating]').val();
        formData.healthPoints = form.find('input[name=healthPoints]').val();
        formData.luckRating = form.find('input[name=luckRating]').val();
        formData.manaPool = form.find('input[name=manaPool]').val();
        formData.powerRating = form.find('input[name=powerRating]').val();
        formData.nextCreature = form.find('select[name=nextEvolution]').val();
        formData.previousCreature = form.find('select[name=previousEvolution]').val();

        this.ajaxHelper.post(url, formData, AjaxHelper.JSON, (data, textStatus, jqXHR) => {
            this.beginMediation();
        });
    }
}