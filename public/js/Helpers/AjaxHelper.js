class AjaxHelper {
    constructor() {
        Object.defineProperty(this, 'JSON', {
            value: 'json',
            writable : false,
            enumerable : false,
            configurable : false
        });
        Object.defineProperty(this, 'HTML', {
            value: 'html',
            writable : false,
            enumerable : false,
            configurable : false
        });
    }

    get(url, data, dataType, successCallback, failureCallback) {
        $.ajax(url, {
            data: data,
            dataType: dataType,
            error: failureCallback,
            success: successCallback,
            type: "GET"
        });
    }

    post(url, data, dataType, successCallback, failureCallback) {
        $.ajax(url, {
            data: data,
            dataType: dataType,
            error: failureCallback,
            success: successCallback,
            type: "POST"
        });
    }
}